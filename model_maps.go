/*
	Transforming ItemsIn -> Items -> ItemsOut
	Where Items has column values ar integers to save memmory
	maps are needed to restore integers back to the actual values.
	those are generated and stored here.
*/

package main

type ModelMaps struct {
	Hoofdact                   MappedColumn
	Nevact1                    MappedColumn
	Nevact2                    MappedColumn
	RvFijn                     MappedColumn
	Buurtnaam                  MappedColumn
	Buurtcode                  MappedColumn
	Gemeentenaam               MappedColumn
	Gemeentecode               MappedColumn
	Provincienaam              MappedColumn
	Provinciecode              MappedColumn
	OmschrijvingRechtsvormcode MappedColumn
	Sbicode                    MappedColumn
	Activiteit                 MappedColumn
	L1Code                     MappedColumn
	L1Title                    MappedColumn
	L2Code                     MappedColumn
	L2Title                    MappedColumn
	L3Code                     MappedColumn
	L3Title                    MappedColumn
	L4Code                     MappedColumn
	L4Title                    MappedColumn
	L5Code                     MappedColumn
	L5Title                    MappedColumn
}

var BitArrays map[string]fieldBitarrayMap

var Hoofdact MappedColumn
var Nevact1 MappedColumn
var Nevact2 MappedColumn
var RvFijn MappedColumn
var Buurtnaam MappedColumn
var Buurtcode MappedColumn
var Gemeentenaam MappedColumn
var Gemeentecode MappedColumn
var Provincienaam MappedColumn
var Provinciecode MappedColumn
var OmschrijvingRechtsvormcode MappedColumn
var Sbicode MappedColumn
var Activiteit MappedColumn
var L1Code MappedColumn
var L1Title MappedColumn
var L2Code MappedColumn
var L2Title MappedColumn
var L3Code MappedColumn
var L3Title MappedColumn
var L4Code MappedColumn
var L4Title MappedColumn
var L5Code MappedColumn
var L5Title MappedColumn

func clearBitArrays() {
	BitArrays = make(map[string]fieldBitarrayMap)
}

func init() {
	clearBitArrays()
	setUpRepeatedColumns()
}

func setUpRepeatedColumns() {
	Hoofdact = NewReapeatedColumn("hoofdact")
	Nevact1 = NewReapeatedColumn("nevact1")
	Nevact2 = NewReapeatedColumn("nevact2")
	RvFijn = NewReapeatedColumn("rv_fijn")
	Buurtnaam = NewReapeatedColumn("buurtnaam")
	Buurtcode = NewReapeatedColumn("buurtcode")
	Gemeentenaam = NewReapeatedColumn("gemeentenaam")
	Gemeentecode = NewReapeatedColumn("gemeentecode")
	Provincienaam = NewReapeatedColumn("provincienaam")
	Provinciecode = NewReapeatedColumn("provinciecode")
	OmschrijvingRechtsvormcode = NewReapeatedColumn("omschrijving_rechtsvormcode")
	Sbicode = NewReapeatedColumn("sbicode")
	Activiteit = NewReapeatedColumn("activiteit")
	L1Code = NewReapeatedColumn("l1_code")
	L1Title = NewReapeatedColumn("l1_title")
	L2Code = NewReapeatedColumn("l2_code")
	L2Title = NewReapeatedColumn("l2_title")
	L3Code = NewReapeatedColumn("l3_code")
	L3Title = NewReapeatedColumn("l3_title")
	L4Code = NewReapeatedColumn("l4_code")
	L4Title = NewReapeatedColumn("l4_title")
	L5Code = NewReapeatedColumn("l5_code")
	L5Title = NewReapeatedColumn("l5_title")

}

func CreateMapstore() ModelMaps {
	return ModelMaps{
		Hoofdact,
		Nevact1,
		Nevact2,
		RvFijn,
		Buurtnaam,
		Buurtcode,
		Gemeentenaam,
		Gemeentecode,
		Provincienaam,
		Provinciecode,
		OmschrijvingRechtsvormcode,
		Sbicode,
		Activiteit,
		L1Code,
		L1Title,
		L2Code,
		L2Title,
		L3Code,
		L3Title,
		L4Code,
		L4Title,
		L5Code,
		L5Title,
	}
}

func LoadMapstore(m ModelMaps) {

	Hoofdact = m.Hoofdact
	Nevact1 = m.Nevact1
	Nevact2 = m.Nevact2
	RvFijn = m.RvFijn
	Buurtnaam = m.Buurtnaam
	Buurtcode = m.Buurtcode
	Gemeentenaam = m.Gemeentenaam
	Gemeentecode = m.Gemeentecode
	Provincienaam = m.Provincienaam
	Provinciecode = m.Provinciecode
	OmschrijvingRechtsvormcode = m.OmschrijvingRechtsvormcode
	Sbicode = m.Sbicode
	Activiteit = m.Activiteit
	L1Code = m.L1Code
	L1Title = m.L1Title
	L2Code = m.L2Code
	L2Title = m.L2Title
	L3Code = m.L3Code
	L3Title = m.L3Title
	L4Code = m.L4Code
	L4Title = m.L4Title
	L5Code = m.L5Code
	L5Title = m.L5Title

	RegisteredColumns[Hoofdact.Name] = Hoofdact
	RegisteredColumns[Nevact1.Name] = Nevact1
	RegisteredColumns[Nevact2.Name] = Nevact2
	RegisteredColumns[RvFijn.Name] = RvFijn
	RegisteredColumns[Buurtnaam.Name] = Buurtnaam
	RegisteredColumns[Buurtcode.Name] = Buurtcode
	RegisteredColumns[Gemeentenaam.Name] = Gemeentenaam
	RegisteredColumns[Gemeentecode.Name] = Gemeentecode
	RegisteredColumns[Provincienaam.Name] = Provincienaam
	RegisteredColumns[Provinciecode.Name] = Provinciecode
	RegisteredColumns[OmschrijvingRechtsvormcode.Name] = OmschrijvingRechtsvormcode
	RegisteredColumns[Sbicode.Name] = Sbicode
	RegisteredColumns[Activiteit.Name] = Activiteit
	RegisteredColumns[L1Code.Name] = L1Code
	RegisteredColumns[L1Title.Name] = L1Title
	RegisteredColumns[L2Code.Name] = L2Code
	RegisteredColumns[L2Title.Name] = L2Title
	RegisteredColumns[L3Code.Name] = L3Code
	RegisteredColumns[L3Title.Name] = L3Title
	RegisteredColumns[L4Code.Name] = L4Code
	RegisteredColumns[L4Title.Name] = L4Title
	RegisteredColumns[L5Code.Name] = L5Code
	RegisteredColumns[L5Title.Name] = L5Title

}
