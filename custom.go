package main

import (
	"fmt"
	"strconv"
)

type registerCustomGroupByFunc map[string]func(*Item, ItemsGroupedBy)

var RegisterGroupByCustom registerCustomGroupByFunc

func init() {
	RegisterGroupByCustom = make(registerCustomGroupByFunc)
}

func (i Item) Pretty() string {
	return fmt.Sprintf("%s - %s - %s", i.Dossiernr, i.Vgnummer, i.Hn1X45)
}

func reduceVestigingen(items Items) map[string]string {

	result := make(map[string]string)

	seen := make(map[string]int)

	vestigingen := 0

	for i := range items {
		_, ok := seen[items[i].Vgnummer]
		if ok {
			//seen
			continue

		}
		// not seen add to seen
		vestigingen += 1
		seen[items[i].Vgnummer] += 1
	}
	result["vestigingen"] = strconv.Itoa(vestigingen)
	return result
}

/*

 */
