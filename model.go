/*
	model.go define the 'items' to store.
	All columns with getters and setters are defined here.

	ItemIn, represent rows from the Input data
	Item, the compact item stored in memmory
	ItemOut, defines how and which fields are exported out
	of the API. It is possible to ignore input columns

	Repeated values are stored in maps with int numbers
	as keys.  Optionally bitarrays are created for reapeated
	column values to do fast bit-wise filtering.

	A S2 geo index in created for lat, lon values.

	Unique values are stored as-is.

	The generated codes leaves room to create custom
	index functions yourself to create an API with an
	< 1 ms response time for your specific needs.

	This codebase solves: I need to have an API on this
	tabular dataset fast!
*/

package main

import (
	"encoding/json"
	"errors"
	"sort"
	"strconv"
	"strings"

	"github.com/Workiva/go-datastructures/bitarray"
)

type registerGroupByFunc map[string]func(*Item) string
type registerGettersMap map[string]func(*Item) string
type registerReduce map[string]func(Items) map[string]string

type registerBitArray map[string]func(s string) (bitarray.BitArray, error)
type fieldBitarrayMap map[uint32]bitarray.BitArray

type ItemIn struct {
	Dossiernr                  string `json:"dossiernr"`
	Subdossier                 string `json:"subdossier"`
	Vgnummer                   string `json:"vgnummer"`
	Hn1X45                     string `json:"hn_1x45"`
	Hoofdact                   string `json:"hoofdact"`
	Nevact1                    string `json:"nevact1"`
	Nevact2                    string `json:"nevact2"`
	Huisnr                     string `json:"huisnr"`
	ToevHsnr                   string `json:"toev_hsnr"`
	Nm1X45                     string `json:"nm_1x45"`
	DatVest                    string `json:"dat_vest"`
	DatInschr                  string `json:"dat_inschr"`
	DatOprich                  string `json:"dat_oprich"`
	RedInschr                  string `json:"red_inschr"`
	RedUitsch                  string `json:"red_uitsch"`
	RedOpheff                  string `json:"red_opheff"`
	Vennaam                    string `json:"vennaam"`
	Gestortkap                 string `json:"gestortkap"`
	StatInsch                  string `json:"stat_insch"`
	MobielNr                   string `json:"mobiel_nr"`
	RvFijn                     string `json:"rv_fijn"`
	Url                        string `json:"url"`
	Pid                        string `json:"pid"`
	Numid                      string `json:"numid"`
	Vid                        string `json:"vid"`
	Sid                        string `json:"sid"`
	Lid                        string `json:"lid"`
	Huisnummer                 string `json:"huisnummer"`
	Huisletter                 string `json:"huisletter"`
	Huisnummertoevoeging       string `json:"huisnummertoevoeging"`
	Postcode                   string `json:"postcode"`
	Oppervlakte                string `json:"oppervlakte"`
	MatchScore                 string `json:"match_score"`
	Geopunt                    string `json:"geopunt"`
	Buurtnaam                  string `json:"buurtnaam"`
	Buurtcode                  string `json:"buurtcode"`
	Gemeentenaam               string `json:"gemeentenaam"`
	Gemeentecode               string `json:"gemeentecode"`
	Provincienaam              string `json:"provincienaam"`
	Provinciecode              string `json:"provinciecode"`
	OmschrijvingRechtsvormcode string `json:"omschrijving_rechtsvormcode"`
	Sbicode                    string `json:"sbicode"`
	Activiteit                 string `json:"activiteit"`
	L1Code                     string `json:"l1_code"`
	L1Title                    string `json:"l1_title"`
	L2Code                     string `json:"l2_code"`
	L2Title                    string `json:"l2_title"`
	L3Code                     string `json:"l3_code"`
	L3Title                    string `json:"l3_title"`
	L4Code                     string `json:"l4_code"`
	L4Title                    string `json:"l4_title"`
	L5Code                     string `json:"l5_code"`
	L5Title                    string `json:"l5_title"`
	Id                         string `json:"id"`
}

type ItemOut struct {
	Dossiernr                  string `json:"dossiernr"`
	Subdossier                 string `json:"subdossier"`
	Vgnummer                   string `json:"vgnummer"`
	Hn1X45                     string `json:"hn_1x45"`
	Hoofdact                   string `json:"hoofdact"`
	Nevact1                    string `json:"nevact1"`
	Nevact2                    string `json:"nevact2"`
	Huisnr                     string `json:"huisnr"`
	ToevHsnr                   string `json:"toev_hsnr"`
	Nm1X45                     string `json:"nm_1x45"`
	DatVest                    string `json:"dat_vest"`
	DatInschr                  string `json:"dat_inschr"`
	DatOprich                  string `json:"dat_oprich"`
	RedInschr                  string `json:"red_inschr"`
	RedUitsch                  string `json:"red_uitsch"`
	RedOpheff                  string `json:"red_opheff"`
	Vennaam                    string `json:"vennaam"`
	Gestortkap                 string `json:"gestortkap"`
	StatInsch                  string `json:"stat_insch"`
	RvFijn                     string `json:"rv_fijn"`
	Url                        string `json:"url"`
	Pid                        string `json:"pid"`
	Numid                      string `json:"numid"`
	Vid                        string `json:"vid"`
	Sid                        string `json:"sid"`
	Lid                        string `json:"lid"`
	Huisnummer                 string `json:"huisnummer"`
	Huisletter                 string `json:"huisletter"`
	Huisnummertoevoeging       string `json:"huisnummertoevoeging"`
	Postcode                   string `json:"postcode"`
	Oppervlakte                string `json:"oppervlakte"`
	MatchScore                 string `json:"match_score"`
	Geopunt                    string `json:"geopunt"`
	Buurtnaam                  string `json:"buurtnaam"`
	Buurtcode                  string `json:"buurtcode"`
	Gemeentenaam               string `json:"gemeentenaam"`
	Gemeentecode               string `json:"gemeentecode"`
	Provincienaam              string `json:"provincienaam"`
	Provinciecode              string `json:"provinciecode"`
	OmschrijvingRechtsvormcode string `json:"omschrijving_rechtsvormcode"`
	Sbicode                    string `json:"sbicode"`
	Activiteit                 string `json:"activiteit"`
	L1Code                     string `json:"l1_code"`
	L1Title                    string `json:"l1_title"`
	L2Code                     string `json:"l2_code"`
	L2Title                    string `json:"l2_title"`
	L3Code                     string `json:"l3_code"`
	L3Title                    string `json:"l3_title"`
	L4Code                     string `json:"l4_code"`
	L4Title                    string `json:"l4_title"`
	L5Code                     string `json:"l5_code"`
	L5Title                    string `json:"l5_title"`
	Id                         string `json:"id"`
}

type Item struct {
	Label                      int // internal index in ITEMS
	Dossiernr                  string
	Subdossier                 string
	Vgnummer                   string
	Hn1X45                     string
	Hoofdact                   uint32
	Nevact1                    uint32
	Nevact2                    uint32
	Huisnr                     string
	ToevHsnr                   string
	Nm1X45                     string
	DatVest                    string
	DatInschr                  string
	DatOprich                  string
	RedInschr                  string
	RedUitsch                  string
	RedOpheff                  string
	Vennaam                    string
	Gestortkap                 string
	StatInsch                  string
	RvFijn                     uint32
	Url                        string
	Pid                        string
	Numid                      string
	Vid                        string
	Sid                        string
	Lid                        string
	Huisnummer                 string
	Huisletter                 string
	Huisnummertoevoeging       string
	Postcode                   string
	Oppervlakte                string
	MatchScore                 string
	Geopunt                    string
	Buurtnaam                  uint32
	Buurtcode                  uint32
	Gemeentenaam               uint32
	Gemeentecode               uint32
	Provincienaam              uint32
	Provinciecode              uint32
	OmschrijvingRechtsvormcode uint32
	Sbicode                    uint32
	Activiteit                 uint32
	L1Code                     uint32
	L1Title                    uint32
	L2Code                     uint32
	L2Title                    uint32
	L3Code                     uint32
	L3Title                    uint32
	L4Code                     uint32
	L4Title                    uint32
	L5Code                     uint32
	L5Title                    uint32
	Id                         string
}

func (i Item) MarshalJSON() ([]byte, error) {
	return json.Marshal(i.Serialize())
}

// Shrink create smaller Item using uint32
func (i ItemIn) Shrink(label int) Item {

	Hoofdact.Store(i.Hoofdact)
	Nevact1.Store(i.Nevact1)
	Nevact2.Store(i.Nevact2)
	RvFijn.Store(i.RvFijn)
	Buurtnaam.Store(i.Buurtnaam)
	Buurtcode.Store(i.Buurtcode)
	Gemeentenaam.Store(i.Gemeentenaam)
	Gemeentecode.Store(i.Gemeentecode)
	Provincienaam.Store(i.Provincienaam)
	Provinciecode.Store(i.Provinciecode)
	OmschrijvingRechtsvormcode.Store(i.OmschrijvingRechtsvormcode)
	Sbicode.Store(i.Sbicode)
	Activiteit.Store(i.Activiteit)
	L1Code.Store(i.L1Code)
	L1Title.Store(i.L1Title)
	L2Code.Store(i.L2Code)
	L2Title.Store(i.L2Title)
	L3Code.Store(i.L3Code)
	L3Title.Store(i.L3Title)
	L4Code.Store(i.L4Code)
	L4Title.Store(i.L4Title)
	L5Code.Store(i.L5Code)
	L5Title.Store(i.L5Title)

	return Item{

		label,

		i.Dossiernr,
		i.Subdossier,
		i.Vgnummer,
		i.Hn1X45,
		Hoofdact.GetIndex(i.Hoofdact),
		Nevact1.GetIndex(i.Nevact1),
		Nevact2.GetIndex(i.Nevact2),
		i.Huisnr,
		i.ToevHsnr,
		i.Nm1X45,
		i.DatVest,
		i.DatInschr,
		i.DatOprich,
		i.RedInschr,
		i.RedUitsch,
		i.RedOpheff,
		i.Vennaam,
		i.Gestortkap,
		i.StatInsch,
		RvFijn.GetIndex(i.RvFijn),
		i.Url,
		i.Pid,
		i.Numid,
		i.Vid,
		i.Sid,
		i.Lid,
		i.Huisnummer,
		i.Huisletter,
		i.Huisnummertoevoeging,
		i.Postcode,
		i.Oppervlakte,
		i.MatchScore,
		i.Geopunt,
		Buurtnaam.GetIndex(i.Buurtnaam),
		Buurtcode.GetIndex(i.Buurtcode),
		Gemeentenaam.GetIndex(i.Gemeentenaam),
		Gemeentecode.GetIndex(i.Gemeentecode),
		Provincienaam.GetIndex(i.Provincienaam),
		Provinciecode.GetIndex(i.Provinciecode),
		OmschrijvingRechtsvormcode.GetIndex(i.OmschrijvingRechtsvormcode),
		Sbicode.GetIndex(i.Sbicode),
		Activiteit.GetIndex(i.Activiteit),
		L1Code.GetIndex(i.L1Code),
		L1Title.GetIndex(i.L1Title),
		L2Code.GetIndex(i.L2Code),
		L2Title.GetIndex(i.L2Title),
		L3Code.GetIndex(i.L3Code),
		L3Title.GetIndex(i.L3Title),
		L4Code.GetIndex(i.L4Code),
		L4Title.GetIndex(i.L4Title),
		L5Code.GetIndex(i.L5Code),
		L5Title.GetIndex(i.L5Title),
		i.Id,
	}
}

// Store selected columns in seperate map[columnvalue]bitarray
// for fast item selection
func (i *Item) StoreBitArrayColumns() {
	SetBitArray("buurtcode", i.Buurtcode, i.Label)
	SetBitArray("gemeentecode", i.Gemeentecode, i.Label)
	SetBitArray("provinciecode", i.Provinciecode, i.Label)
	SetBitArray("sbicode", i.Sbicode, i.Label)
	SetBitArray("activiteit", i.Activiteit, i.Label)
	SetBitArray("l1_code", i.L1Code, i.Label)
	SetBitArray("l1_title", i.L1Title, i.Label)
	SetBitArray("l2_code", i.L2Code, i.Label)
	SetBitArray("l2_title", i.L2Title, i.Label)
	SetBitArray("l3_code", i.L3Code, i.Label)
	SetBitArray("l4_code", i.L4Code, i.Label)

}

func (i Item) Serialize() ItemOut {
	return ItemOut{

		i.Dossiernr,
		i.Subdossier,
		i.Vgnummer,
		i.Hn1X45,
		Hoofdact.GetValue(i.Hoofdact),
		Nevact1.GetValue(i.Nevact1),
		Nevact2.GetValue(i.Nevact2),
		i.Huisnr,
		i.ToevHsnr,
		i.Nm1X45,
		i.DatVest,
		i.DatInschr,
		i.DatOprich,
		i.RedInschr,
		i.RedUitsch,
		i.RedOpheff,
		i.Vennaam,
		i.Gestortkap,
		i.StatInsch,
		RvFijn.GetValue(i.RvFijn),
		i.Url,
		i.Pid,
		i.Numid,
		i.Vid,
		i.Sid,
		i.Lid,
		i.Huisnummer,
		i.Huisletter,
		i.Huisnummertoevoeging,
		i.Postcode,
		i.Oppervlakte,
		i.MatchScore,
		i.Geopunt,
		Buurtnaam.GetValue(i.Buurtnaam),
		Buurtcode.GetValue(i.Buurtcode),
		Gemeentenaam.GetValue(i.Gemeentenaam),
		Gemeentecode.GetValue(i.Gemeentecode),
		Provincienaam.GetValue(i.Provincienaam),
		Provinciecode.GetValue(i.Provinciecode),
		OmschrijvingRechtsvormcode.GetValue(i.OmschrijvingRechtsvormcode),
		Sbicode.GetValue(i.Sbicode),
		Activiteit.GetValue(i.Activiteit),
		L1Code.GetValue(i.L1Code),
		L1Title.GetValue(i.L1Title),
		L2Code.GetValue(i.L2Code),
		L2Title.GetValue(i.L2Title),
		L3Code.GetValue(i.L3Code),
		L3Title.GetValue(i.L3Title),
		L4Code.GetValue(i.L4Code),
		L4Title.GetValue(i.L4Title),
		L5Code.GetValue(i.L5Code),
		L5Title.GetValue(i.L5Title),
		i.Id,
	}
}

func (i ItemIn) Columns() []string {
	return []string{

		"dossiernr",
		"subdossier",
		"vgnummer",
		"hn_1x45",
		"hoofdact",
		"nevact1",
		"nevact2",
		"huisnr",
		"toev_hsnr",
		"nm_1x45",
		"dat_vest",
		"dat_inschr",
		"dat_oprich",
		"red_inschr",
		"red_uitsch",
		"red_opheff",
		"vennaam",
		"gestortkap",
		"stat_insch",
		"mobiel_nr",
		"rv_fijn",
		"url",
		"pid",
		"numid",
		"vid",
		"sid",
		"lid",
		"huisnummer",
		"huisletter",
		"huisnummertoevoeging",
		"postcode",
		"oppervlakte",
		"match_score",
		"geopunt",
		"buurtnaam",
		"buurtcode",
		"gemeentenaam",
		"gemeentecode",
		"provincienaam",
		"provinciecode",
		"omschrijving_rechtsvormcode",
		"geometry",
		"sbicode",
		"activiteit",
		"l1_code",
		"l1_title",
		"l2_code",
		"l2_title",
		"l3_code",
		"l3_title",
		"l4_code",
		"l4_title",
		"l5_code",
		"l5_title",
		"id",
	}
}

func (i ItemOut) Columns() []string {
	return []string{

		"dossiernr",
		"subdossier",
		"vgnummer",
		"hn_1x45",
		"hoofdact",
		"nevact1",
		"nevact2",
		"huisnr",
		"toev_hsnr",
		"nm_1x45",
		"dat_vest",
		"dat_inschr",
		"dat_oprich",
		"red_inschr",
		"red_uitsch",
		"red_opheff",
		"vennaam",
		"gestortkap",
		"stat_insch",
		"rv_fijn",
		"url",
		"pid",
		"numid",
		"vid",
		"sid",
		"lid",
		"huisnummer",
		"huisletter",
		"huisnummertoevoeging",
		"postcode",
		"oppervlakte",
		"match_score",
		"geopunt",
		"buurtnaam",
		"buurtcode",
		"gemeentenaam",
		"gemeentecode",
		"provincienaam",
		"provinciecode",
		"omschrijving_rechtsvormcode",
		"geometry",
		"sbicode",
		"activiteit",
		"l1_code",
		"l1_title",
		"l2_code",
		"l2_title",
		"l3_code",
		"l3_title",
		"l4_code",
		"l4_title",
		"l5_code",
		"l5_title",
		"id",
	}
}

func (i Item) Row() []string {

	return []string{

		i.Dossiernr,
		i.Subdossier,
		i.Vgnummer,
		i.Hn1X45,
		Hoofdact.GetValue(i.Hoofdact),
		Nevact1.GetValue(i.Nevact1),
		Nevact2.GetValue(i.Nevact2),
		i.Huisnr,
		i.ToevHsnr,
		i.Nm1X45,
		i.DatVest,
		i.DatInschr,
		i.DatOprich,
		i.RedInschr,
		i.RedUitsch,
		i.RedOpheff,
		i.Vennaam,
		i.Gestortkap,
		i.StatInsch,
		RvFijn.GetValue(i.RvFijn),
		i.Url,
		i.Pid,
		i.Numid,
		i.Vid,
		i.Sid,
		i.Lid,
		i.Huisnummer,
		i.Huisletter,
		i.Huisnummertoevoeging,
		i.Postcode,
		i.Oppervlakte,
		i.MatchScore,
		i.Geopunt,
		Buurtnaam.GetValue(i.Buurtnaam),
		Buurtcode.GetValue(i.Buurtcode),
		Gemeentenaam.GetValue(i.Gemeentenaam),
		Gemeentecode.GetValue(i.Gemeentecode),
		Provincienaam.GetValue(i.Provincienaam),
		Provinciecode.GetValue(i.Provinciecode),
		OmschrijvingRechtsvormcode.GetValue(i.OmschrijvingRechtsvormcode),
		Sbicode.GetValue(i.Sbicode),
		Activiteit.GetValue(i.Activiteit),
		L1Code.GetValue(i.L1Code),
		L1Title.GetValue(i.L1Title),
		L2Code.GetValue(i.L2Code),
		L2Title.GetValue(i.L2Title),
		L3Code.GetValue(i.L3Code),
		L3Title.GetValue(i.L3Title),
		L4Code.GetValue(i.L4Code),
		L4Title.GetValue(i.L4Title),
		L5Code.GetValue(i.L5Code),
		L5Title.GetValue(i.L5Title),
		i.Id,
	}
}

func (i Item) GetIndex() string {
	return GettersDossiernr(&i)
}

func (i Item) GetGeometry() string {
	return GettersGeopunt(&i)
}

// contain filter Dossiernr
func FilterDossiernrContains(i *Item, s string) bool {
	return strings.Contains(i.Dossiernr, s)
}

// startswith filter Dossiernr
func FilterDossiernrStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(i.Dossiernr, s)
}

// match filters Dossiernr
func FilterDossiernrMatch(i *Item, s string) bool {
	return i.Dossiernr == s
}

// getter Dossiernr
func GettersDossiernr(i *Item) string {
	return i.Dossiernr
}

// contain filter Subdossier
func FilterSubdossierContains(i *Item, s string) bool {
	return strings.Contains(i.Subdossier, s)
}

// startswith filter Subdossier
func FilterSubdossierStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(i.Subdossier, s)
}

// match filters Subdossier
func FilterSubdossierMatch(i *Item, s string) bool {
	return i.Subdossier == s
}

// getter Subdossier
func GettersSubdossier(i *Item) string {
	return i.Subdossier
}

// contain filter Vgnummer
func FilterVgnummerContains(i *Item, s string) bool {
	return strings.Contains(i.Vgnummer, s)
}

// startswith filter Vgnummer
func FilterVgnummerStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(i.Vgnummer, s)
}

// match filters Vgnummer
func FilterVgnummerMatch(i *Item, s string) bool {
	return i.Vgnummer == s
}

// getter Vgnummer
func GettersVgnummer(i *Item) string {
	return i.Vgnummer
}

// contain filter Hn1X45
func FilterHn1X45Contains(i *Item, s string) bool {
	return strings.Contains(i.Hn1X45, s)
}

// startswith filter Hn1X45
func FilterHn1X45StartsWith(i *Item, s string) bool {
	return strings.HasPrefix(i.Hn1X45, s)
}

// match filters Hn1X45
func FilterHn1X45Match(i *Item, s string) bool {
	return i.Hn1X45 == s
}

// getter Hn1X45
func GettersHn1X45(i *Item) string {
	return i.Hn1X45
}

// contain filter Hoofdact
func FilterHoofdactContains(i *Item, s string) bool {
	return strings.Contains(Hoofdact.GetValue(i.Hoofdact), s)
}

// startswith filter Hoofdact
func FilterHoofdactStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(Hoofdact.GetValue(i.Hoofdact), s)
}

// match filters Hoofdact
func FilterHoofdactMatch(i *Item, s string) bool {
	return Hoofdact.GetValue(i.Hoofdact) == s
}

// getter Hoofdact
func GettersHoofdact(i *Item) string {
	return Hoofdact.GetValue(i.Hoofdact)
}

// contain filter Nevact1
func FilterNevact1Contains(i *Item, s string) bool {
	return strings.Contains(Nevact1.GetValue(i.Nevact1), s)
}

// startswith filter Nevact1
func FilterNevact1StartsWith(i *Item, s string) bool {
	return strings.HasPrefix(Nevact1.GetValue(i.Nevact1), s)
}

// match filters Nevact1
func FilterNevact1Match(i *Item, s string) bool {
	return Nevact1.GetValue(i.Nevact1) == s
}

// getter Nevact1
func GettersNevact1(i *Item) string {
	return Nevact1.GetValue(i.Nevact1)
}

// contain filter Nevact2
func FilterNevact2Contains(i *Item, s string) bool {
	return strings.Contains(Nevact2.GetValue(i.Nevact2), s)
}

// startswith filter Nevact2
func FilterNevact2StartsWith(i *Item, s string) bool {
	return strings.HasPrefix(Nevact2.GetValue(i.Nevact2), s)
}

// match filters Nevact2
func FilterNevact2Match(i *Item, s string) bool {
	return Nevact2.GetValue(i.Nevact2) == s
}

// getter Nevact2
func GettersNevact2(i *Item) string {
	return Nevact2.GetValue(i.Nevact2)
}

// contain filter Huisnr
func FilterHuisnrContains(i *Item, s string) bool {
	return strings.Contains(i.Huisnr, s)
}

// startswith filter Huisnr
func FilterHuisnrStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(i.Huisnr, s)
}

// match filters Huisnr
func FilterHuisnrMatch(i *Item, s string) bool {
	return i.Huisnr == s
}

// getter Huisnr
func GettersHuisnr(i *Item) string {
	return i.Huisnr
}

// contain filter ToevHsnr
func FilterToevHsnrContains(i *Item, s string) bool {
	return strings.Contains(i.ToevHsnr, s)
}

// startswith filter ToevHsnr
func FilterToevHsnrStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(i.ToevHsnr, s)
}

// match filters ToevHsnr
func FilterToevHsnrMatch(i *Item, s string) bool {
	return i.ToevHsnr == s
}

// getter ToevHsnr
func GettersToevHsnr(i *Item) string {
	return i.ToevHsnr
}

// contain filter Nm1X45
func FilterNm1X45Contains(i *Item, s string) bool {
	return strings.Contains(i.Nm1X45, s)
}

// startswith filter Nm1X45
func FilterNm1X45StartsWith(i *Item, s string) bool {
	return strings.HasPrefix(i.Nm1X45, s)
}

// match filters Nm1X45
func FilterNm1X45Match(i *Item, s string) bool {
	return i.Nm1X45 == s
}

// getter Nm1X45
func GettersNm1X45(i *Item) string {
	return i.Nm1X45
}

// contain filter DatVest
func FilterDatVestContains(i *Item, s string) bool {
	return strings.Contains(i.DatVest, s)
}

// startswith filter DatVest
func FilterDatVestStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(i.DatVest, s)
}

// match filters DatVest
func FilterDatVestMatch(i *Item, s string) bool {
	return i.DatVest == s
}

// getter DatVest
func GettersDatVest(i *Item) string {
	return i.DatVest
}

// contain filter DatInschr
func FilterDatInschrContains(i *Item, s string) bool {
	return strings.Contains(i.DatInschr, s)
}

// startswith filter DatInschr
func FilterDatInschrStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(i.DatInschr, s)
}

// match filters DatInschr
func FilterDatInschrMatch(i *Item, s string) bool {
	return i.DatInschr == s
}

// getter DatInschr
func GettersDatInschr(i *Item) string {
	return i.DatInschr
}

// contain filter DatOprich
func FilterDatOprichContains(i *Item, s string) bool {
	return strings.Contains(i.DatOprich, s)
}

// startswith filter DatOprich
func FilterDatOprichStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(i.DatOprich, s)
}

// match filters DatOprich
func FilterDatOprichMatch(i *Item, s string) bool {
	return i.DatOprich == s
}

// getter DatOprich
func GettersDatOprich(i *Item) string {
	return i.DatOprich
}

// contain filter RedInschr
func FilterRedInschrContains(i *Item, s string) bool {
	return strings.Contains(i.RedInschr, s)
}

// startswith filter RedInschr
func FilterRedInschrStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(i.RedInschr, s)
}

// match filters RedInschr
func FilterRedInschrMatch(i *Item, s string) bool {
	return i.RedInschr == s
}

// getter RedInschr
func GettersRedInschr(i *Item) string {
	return i.RedInschr
}

// contain filter RedUitsch
func FilterRedUitschContains(i *Item, s string) bool {
	return strings.Contains(i.RedUitsch, s)
}

// startswith filter RedUitsch
func FilterRedUitschStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(i.RedUitsch, s)
}

// match filters RedUitsch
func FilterRedUitschMatch(i *Item, s string) bool {
	return i.RedUitsch == s
}

// getter RedUitsch
func GettersRedUitsch(i *Item) string {
	return i.RedUitsch
}

// contain filter RedOpheff
func FilterRedOpheffContains(i *Item, s string) bool {
	return strings.Contains(i.RedOpheff, s)
}

// startswith filter RedOpheff
func FilterRedOpheffStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(i.RedOpheff, s)
}

// match filters RedOpheff
func FilterRedOpheffMatch(i *Item, s string) bool {
	return i.RedOpheff == s
}

// getter RedOpheff
func GettersRedOpheff(i *Item) string {
	return i.RedOpheff
}

// contain filter Vennaam
func FilterVennaamContains(i *Item, s string) bool {
	return strings.Contains(i.Vennaam, s)
}

// startswith filter Vennaam
func FilterVennaamStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(i.Vennaam, s)
}

// match filters Vennaam
func FilterVennaamMatch(i *Item, s string) bool {
	return i.Vennaam == s
}

// getter Vennaam
func GettersVennaam(i *Item) string {
	return i.Vennaam
}

// contain filter Gestortkap
func FilterGestortkapContains(i *Item, s string) bool {
	return strings.Contains(i.Gestortkap, s)
}

// startswith filter Gestortkap
func FilterGestortkapStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(i.Gestortkap, s)
}

// match filters Gestortkap
func FilterGestortkapMatch(i *Item, s string) bool {
	return i.Gestortkap == s
}

// getter Gestortkap
func GettersGestortkap(i *Item) string {
	return i.Gestortkap
}

// contain filter StatInsch
func FilterStatInschContains(i *Item, s string) bool {
	return strings.Contains(i.StatInsch, s)
}

// startswith filter StatInsch
func FilterStatInschStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(i.StatInsch, s)
}

// match filters StatInsch
func FilterStatInschMatch(i *Item, s string) bool {
	return i.StatInsch == s
}

// getter StatInsch
func GettersStatInsch(i *Item) string {
	return i.StatInsch
}

// contain filter RvFijn
func FilterRvFijnContains(i *Item, s string) bool {
	return strings.Contains(RvFijn.GetValue(i.RvFijn), s)
}

// startswith filter RvFijn
func FilterRvFijnStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(RvFijn.GetValue(i.RvFijn), s)
}

// match filters RvFijn
func FilterRvFijnMatch(i *Item, s string) bool {
	return RvFijn.GetValue(i.RvFijn) == s
}

// getter RvFijn
func GettersRvFijn(i *Item) string {
	return RvFijn.GetValue(i.RvFijn)
}

// contain filter Url
func FilterUrlContains(i *Item, s string) bool {
	return strings.Contains(i.Url, s)
}

// startswith filter Url
func FilterUrlStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(i.Url, s)
}

// match filters Url
func FilterUrlMatch(i *Item, s string) bool {
	return i.Url == s
}

// getter Url
func GettersUrl(i *Item) string {
	return i.Url
}

// contain filter Pid
func FilterPidContains(i *Item, s string) bool {
	return strings.Contains(i.Pid, s)
}

// startswith filter Pid
func FilterPidStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(i.Pid, s)
}

// match filters Pid
func FilterPidMatch(i *Item, s string) bool {
	return i.Pid == s
}

// getter Pid
func GettersPid(i *Item) string {
	return i.Pid
}

// contain filter Numid
func FilterNumidContains(i *Item, s string) bool {
	return strings.Contains(i.Numid, s)
}

// startswith filter Numid
func FilterNumidStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(i.Numid, s)
}

// match filters Numid
func FilterNumidMatch(i *Item, s string) bool {
	return i.Numid == s
}

// getter Numid
func GettersNumid(i *Item) string {
	return i.Numid
}

// contain filter Vid
func FilterVidContains(i *Item, s string) bool {
	return strings.Contains(i.Vid, s)
}

// startswith filter Vid
func FilterVidStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(i.Vid, s)
}

// match filters Vid
func FilterVidMatch(i *Item, s string) bool {
	return i.Vid == s
}

// getter Vid
func GettersVid(i *Item) string {
	return i.Vid
}

// contain filter Sid
func FilterSidContains(i *Item, s string) bool {
	return strings.Contains(i.Sid, s)
}

// startswith filter Sid
func FilterSidStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(i.Sid, s)
}

// match filters Sid
func FilterSidMatch(i *Item, s string) bool {
	return i.Sid == s
}

// getter Sid
func GettersSid(i *Item) string {
	return i.Sid
}

// contain filter Lid
func FilterLidContains(i *Item, s string) bool {
	return strings.Contains(i.Lid, s)
}

// startswith filter Lid
func FilterLidStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(i.Lid, s)
}

// match filters Lid
func FilterLidMatch(i *Item, s string) bool {
	return i.Lid == s
}

// getter Lid
func GettersLid(i *Item) string {
	return i.Lid
}

// contain filter Huisnummer
func FilterHuisnummerContains(i *Item, s string) bool {
	return strings.Contains(i.Huisnummer, s)
}

// startswith filter Huisnummer
func FilterHuisnummerStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(i.Huisnummer, s)
}

// match filters Huisnummer
func FilterHuisnummerMatch(i *Item, s string) bool {
	return i.Huisnummer == s
}

// getter Huisnummer
func GettersHuisnummer(i *Item) string {
	return i.Huisnummer
}

// contain filter Huisletter
func FilterHuisletterContains(i *Item, s string) bool {
	return strings.Contains(i.Huisletter, s)
}

// startswith filter Huisletter
func FilterHuisletterStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(i.Huisletter, s)
}

// match filters Huisletter
func FilterHuisletterMatch(i *Item, s string) bool {
	return i.Huisletter == s
}

// getter Huisletter
func GettersHuisletter(i *Item) string {
	return i.Huisletter
}

// contain filter Huisnummertoevoeging
func FilterHuisnummertoevoegingContains(i *Item, s string) bool {
	return strings.Contains(i.Huisnummertoevoeging, s)
}

// startswith filter Huisnummertoevoeging
func FilterHuisnummertoevoegingStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(i.Huisnummertoevoeging, s)
}

// match filters Huisnummertoevoeging
func FilterHuisnummertoevoegingMatch(i *Item, s string) bool {
	return i.Huisnummertoevoeging == s
}

// getter Huisnummertoevoeging
func GettersHuisnummertoevoeging(i *Item) string {
	return i.Huisnummertoevoeging
}

// contain filter Postcode
func FilterPostcodeContains(i *Item, s string) bool {
	return strings.Contains(i.Postcode, s)
}

// startswith filter Postcode
func FilterPostcodeStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(i.Postcode, s)
}

// match filters Postcode
func FilterPostcodeMatch(i *Item, s string) bool {
	return i.Postcode == s
}

// getter Postcode
func GettersPostcode(i *Item) string {
	return i.Postcode
}

// contain filter Oppervlakte
func FilterOppervlakteContains(i *Item, s string) bool {
	return strings.Contains(i.Oppervlakte, s)
}

// startswith filter Oppervlakte
func FilterOppervlakteStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(i.Oppervlakte, s)
}

// match filters Oppervlakte
func FilterOppervlakteMatch(i *Item, s string) bool {
	return i.Oppervlakte == s
}

// getter Oppervlakte
func GettersOppervlakte(i *Item) string {
	return i.Oppervlakte
}

// contain filter MatchScore
func FilterMatchScoreContains(i *Item, s string) bool {
	return strings.Contains(i.MatchScore, s)
}

// startswith filter MatchScore
func FilterMatchScoreStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(i.MatchScore, s)
}

// match filters MatchScore
func FilterMatchScoreMatch(i *Item, s string) bool {
	return i.MatchScore == s
}

// getter MatchScore
func GettersMatchScore(i *Item) string {
	return i.MatchScore
}

// contain filter Geopunt
func FilterGeopuntContains(i *Item, s string) bool {
	return strings.Contains(i.Geopunt, s)
}

// startswith filter Geopunt
func FilterGeopuntStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(i.Geopunt, s)
}

// match filters Geopunt
func FilterGeopuntMatch(i *Item, s string) bool {
	return i.Geopunt == s
}

// getter Geopunt
func GettersGeopunt(i *Item) string {
	return i.Geopunt
}

// contain filter Buurtnaam
func FilterBuurtnaamContains(i *Item, s string) bool {
	return strings.Contains(Buurtnaam.GetValue(i.Buurtnaam), s)
}

// startswith filter Buurtnaam
func FilterBuurtnaamStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(Buurtnaam.GetValue(i.Buurtnaam), s)
}

// match filters Buurtnaam
func FilterBuurtnaamMatch(i *Item, s string) bool {
	return Buurtnaam.GetValue(i.Buurtnaam) == s
}

// getter Buurtnaam
func GettersBuurtnaam(i *Item) string {
	return Buurtnaam.GetValue(i.Buurtnaam)
}

// contain filter Buurtcode
func FilterBuurtcodeContains(i *Item, s string) bool {
	return strings.Contains(Buurtcode.GetValue(i.Buurtcode), s)
}

// startswith filter Buurtcode
func FilterBuurtcodeStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(Buurtcode.GetValue(i.Buurtcode), s)
}

// match filters Buurtcode
func FilterBuurtcodeMatch(i *Item, s string) bool {
	return Buurtcode.GetValue(i.Buurtcode) == s
}

// getter Buurtcode
func GettersBuurtcode(i *Item) string {
	return Buurtcode.GetValue(i.Buurtcode)
}

// contain filter Gemeentenaam
func FilterGemeentenaamContains(i *Item, s string) bool {
	return strings.Contains(Gemeentenaam.GetValue(i.Gemeentenaam), s)
}

// startswith filter Gemeentenaam
func FilterGemeentenaamStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(Gemeentenaam.GetValue(i.Gemeentenaam), s)
}

// match filters Gemeentenaam
func FilterGemeentenaamMatch(i *Item, s string) bool {
	return Gemeentenaam.GetValue(i.Gemeentenaam) == s
}

// getter Gemeentenaam
func GettersGemeentenaam(i *Item) string {
	return Gemeentenaam.GetValue(i.Gemeentenaam)
}

// contain filter Gemeentecode
func FilterGemeentecodeContains(i *Item, s string) bool {
	return strings.Contains(Gemeentecode.GetValue(i.Gemeentecode), s)
}

// startswith filter Gemeentecode
func FilterGemeentecodeStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(Gemeentecode.GetValue(i.Gemeentecode), s)
}

// match filters Gemeentecode
func FilterGemeentecodeMatch(i *Item, s string) bool {
	return Gemeentecode.GetValue(i.Gemeentecode) == s
}

// getter Gemeentecode
func GettersGemeentecode(i *Item) string {
	return Gemeentecode.GetValue(i.Gemeentecode)
}

// contain filter Provincienaam
func FilterProvincienaamContains(i *Item, s string) bool {
	return strings.Contains(Provincienaam.GetValue(i.Provincienaam), s)
}

// startswith filter Provincienaam
func FilterProvincienaamStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(Provincienaam.GetValue(i.Provincienaam), s)
}

// match filters Provincienaam
func FilterProvincienaamMatch(i *Item, s string) bool {
	return Provincienaam.GetValue(i.Provincienaam) == s
}

// getter Provincienaam
func GettersProvincienaam(i *Item) string {
	return Provincienaam.GetValue(i.Provincienaam)
}

// contain filter Provinciecode
func FilterProvinciecodeContains(i *Item, s string) bool {
	return strings.Contains(Provinciecode.GetValue(i.Provinciecode), s)
}

// startswith filter Provinciecode
func FilterProvinciecodeStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(Provinciecode.GetValue(i.Provinciecode), s)
}

// match filters Provinciecode
func FilterProvinciecodeMatch(i *Item, s string) bool {
	return Provinciecode.GetValue(i.Provinciecode) == s
}

// getter Provinciecode
func GettersProvinciecode(i *Item) string {
	return Provinciecode.GetValue(i.Provinciecode)
}

// contain filter OmschrijvingRechtsvormcode
func FilterOmschrijvingRechtsvormcodeContains(i *Item, s string) bool {
	return strings.Contains(OmschrijvingRechtsvormcode.GetValue(i.OmschrijvingRechtsvormcode), s)
}

// startswith filter OmschrijvingRechtsvormcode
func FilterOmschrijvingRechtsvormcodeStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(OmschrijvingRechtsvormcode.GetValue(i.OmschrijvingRechtsvormcode), s)
}

// match filters OmschrijvingRechtsvormcode
func FilterOmschrijvingRechtsvormcodeMatch(i *Item, s string) bool {
	return OmschrijvingRechtsvormcode.GetValue(i.OmschrijvingRechtsvormcode) == s
}

// getter OmschrijvingRechtsvormcode
func GettersOmschrijvingRechtsvormcode(i *Item) string {
	return OmschrijvingRechtsvormcode.GetValue(i.OmschrijvingRechtsvormcode)
}

// contain filter Geometry
// func FilterGeometryContains(i *Item, s string) bool {
// 	return strings.Contains(i.Geometry, s)
// }
//
// // startswith filter Geometry
// func FilterGeometryStartsWith(i *Item, s string) bool {
// 	return strings.HasPrefix(i.Geometry, s)
// }
//
// // match filters Geometry
// func FilterGeometryMatch(i *Item, s string) bool {
// 	return i.Geometry == s
// }
//
// // getter Geometry
// func GettersGeometry(i *Item) string {
// 	return i.Geometry
// }

// contain filter Sbicode
func FilterSbicodeContains(i *Item, s string) bool {
	return strings.Contains(Sbicode.GetValue(i.Sbicode), s)
}

// startswith filter Sbicode
func FilterSbicodeStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(Sbicode.GetValue(i.Sbicode), s)
}

// match filters Sbicode
func FilterSbicodeMatch(i *Item, s string) bool {
	return Sbicode.GetValue(i.Sbicode) == s
}

// getter Sbicode
func GettersSbicode(i *Item) string {
	return Sbicode.GetValue(i.Sbicode)
}

// contain filter Activiteit
func FilterActiviteitContains(i *Item, s string) bool {
	return strings.Contains(Activiteit.GetValue(i.Activiteit), s)
}

// startswith filter Activiteit
func FilterActiviteitStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(Activiteit.GetValue(i.Activiteit), s)
}

// match filters Activiteit
func FilterActiviteitMatch(i *Item, s string) bool {
	return Activiteit.GetValue(i.Activiteit) == s
}

// getter Activiteit
func GettersActiviteit(i *Item) string {
	return Activiteit.GetValue(i.Activiteit)
}

// contain filter L1Code
func FilterL1CodeContains(i *Item, s string) bool {
	return strings.Contains(L1Code.GetValue(i.L1Code), s)
}

// startswith filter L1Code
func FilterL1CodeStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(L1Code.GetValue(i.L1Code), s)
}

// match filters L1Code
func FilterL1CodeMatch(i *Item, s string) bool {
	return L1Code.GetValue(i.L1Code) == s
}

// getter L1Code
func GettersL1Code(i *Item) string {
	return L1Code.GetValue(i.L1Code)
}

// contain filter L1Title
func FilterL1TitleContains(i *Item, s string) bool {
	return strings.Contains(L1Title.GetValue(i.L1Title), s)
}

// startswith filter L1Title
func FilterL1TitleStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(L1Title.GetValue(i.L1Title), s)
}

// match filters L1Title
func FilterL1TitleMatch(i *Item, s string) bool {
	return L1Title.GetValue(i.L1Title) == s
}

// getter L1Title
func GettersL1Title(i *Item) string {
	return L1Title.GetValue(i.L1Title)
}

// contain filter L2Code
func FilterL2CodeContains(i *Item, s string) bool {
	return strings.Contains(L2Code.GetValue(i.L2Code), s)
}

// startswith filter L2Code
func FilterL2CodeStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(L2Code.GetValue(i.L2Code), s)
}

// match filters L2Code
func FilterL2CodeMatch(i *Item, s string) bool {
	return L2Code.GetValue(i.L2Code) == s
}

// getter L2Code
func GettersL2Code(i *Item) string {
	return L2Code.GetValue(i.L2Code)
}

// contain filter L2Title
func FilterL2TitleContains(i *Item, s string) bool {
	return strings.Contains(L2Title.GetValue(i.L2Title), s)
}

// startswith filter L2Title
func FilterL2TitleStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(L2Title.GetValue(i.L2Title), s)
}

// match filters L2Title
func FilterL2TitleMatch(i *Item, s string) bool {
	return L2Title.GetValue(i.L2Title) == s
}

// getter L2Title
func GettersL2Title(i *Item) string {
	return L2Title.GetValue(i.L2Title)
}

// contain filter L3Code
func FilterL3CodeContains(i *Item, s string) bool {
	return strings.Contains(L3Code.GetValue(i.L3Code), s)
}

// startswith filter L3Code
func FilterL3CodeStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(L3Code.GetValue(i.L3Code), s)
}

// match filters L3Code
func FilterL3CodeMatch(i *Item, s string) bool {
	return L3Code.GetValue(i.L3Code) == s
}

// getter L3Code
func GettersL3Code(i *Item) string {
	return L3Code.GetValue(i.L3Code)
}

// contain filter L3Title
func FilterL3TitleContains(i *Item, s string) bool {
	return strings.Contains(L3Title.GetValue(i.L3Title), s)
}

// startswith filter L3Title
func FilterL3TitleStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(L3Title.GetValue(i.L3Title), s)
}

// match filters L3Title
func FilterL3TitleMatch(i *Item, s string) bool {
	return L3Title.GetValue(i.L3Title) == s
}

// getter L3Title
func GettersL3Title(i *Item) string {
	return L3Title.GetValue(i.L3Title)
}

// contain filter L4Code
func FilterL4CodeContains(i *Item, s string) bool {
	return strings.Contains(L4Code.GetValue(i.L4Code), s)
}

// startswith filter L4Code
func FilterL4CodeStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(L4Code.GetValue(i.L4Code), s)
}

// match filters L4Code
func FilterL4CodeMatch(i *Item, s string) bool {
	return L4Code.GetValue(i.L4Code) == s
}

// getter L4Code
func GettersL4Code(i *Item) string {
	return L4Code.GetValue(i.L4Code)
}

// contain filter L4Title
func FilterL4TitleContains(i *Item, s string) bool {
	return strings.Contains(L4Title.GetValue(i.L4Title), s)
}

// startswith filter L4Title
func FilterL4TitleStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(L4Title.GetValue(i.L4Title), s)
}

// match filters L4Title
func FilterL4TitleMatch(i *Item, s string) bool {
	return L4Title.GetValue(i.L4Title) == s
}

// getter L4Title
func GettersL4Title(i *Item) string {
	return L4Title.GetValue(i.L4Title)
}

// contain filter L5Code
func FilterL5CodeContains(i *Item, s string) bool {
	return strings.Contains(L5Code.GetValue(i.L5Code), s)
}

// startswith filter L5Code
func FilterL5CodeStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(L5Code.GetValue(i.L5Code), s)
}

// match filters L5Code
func FilterL5CodeMatch(i *Item, s string) bool {
	return L5Code.GetValue(i.L5Code) == s
}

// getter L5Code
func GettersL5Code(i *Item) string {
	return L5Code.GetValue(i.L5Code)
}

// contain filter L5Title
func FilterL5TitleContains(i *Item, s string) bool {
	return strings.Contains(L5Title.GetValue(i.L5Title), s)
}

// startswith filter L5Title
func FilterL5TitleStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(L5Title.GetValue(i.L5Title), s)
}

// match filters L5Title
func FilterL5TitleMatch(i *Item, s string) bool {
	return L5Title.GetValue(i.L5Title) == s
}

// getter L5Title
func GettersL5Title(i *Item) string {
	return L5Title.GetValue(i.L5Title)
}

// contain filter Id
func FilterIdContains(i *Item, s string) bool {
	return strings.Contains(i.Id, s)
}

// startswith filter Id
func FilterIdStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(i.Id, s)
}

// match filters Id
func FilterIdMatch(i *Item, s string) bool {
	return i.Id == s
}

// getter Id
func GettersId(i *Item) string {
	return i.Id
}

/*
// contain filters
func FilterEkeyContains(i *Item, s string) bool {
	return strings.Contains(i.Ekey, s)
}


// startswith filters
func FilterEkeyStartsWith(i *Item, s string) bool {
	return strings.HasPrefix(i.Ekey, s)
}


// match filters
func FilterEkeyMatch(i *Item, s string) bool {
	return i.Ekey == s
}

// getters
func GettersEkey(i *Item) string {
	return i.Ekey
}
*/

// reduce functions
func reduceCount(items Items) map[string]string {
	result := make(map[string]string)
	result["count"] = strconv.Itoa(len(items))
	return result
}

type GroupedOperations struct {
	Funcs     registerFuncType
	GroupBy   registerGroupByFunc
	Getters   registerGettersMap
	Reduce    registerReduce
	BitArrays registerBitArray
}

var Operations GroupedOperations

var RegisterFuncMap registerFuncType
var RegisterGroupBy registerGroupByFunc
var RegisterGetters registerGettersMap
var RegisterReduce registerReduce
var RegisterBitArray registerBitArray

// ValidateRegsiters validate exposed columns do match filter names
func validateRegisters() error {
	var i = ItemOut{}
	var filters = []string{"match", "contains", "startswith"}
	for _, c := range i.Columns() {
		for _, f := range filters {
			if _, ok := RegisterFuncMap[f+"-"+c]; !ok {
				return errors.New(c + " is missing in RegisterMap")
			}
		}
	}
	return nil
}

func init() {

	RegisterFuncMap = make(registerFuncType)
	RegisterGroupBy = make(registerGroupByFunc)
	RegisterGetters = make(registerGettersMap)
	RegisterReduce = make(registerReduce)

	// register search filter.
	//RegisterFuncMap["search"] = 'EDITYOURSELF'
	// example RegisterFuncMap["search"] = FilterEkeyStartsWith

	//RegisterFuncMap["value"] = 'EDITYOURSELF'
	// example RegisterGetters["value"] = GettersEkey

	// register filters

	//register filters for Dossiernr
	RegisterFuncMap["match-dossiernr"] = FilterDossiernrMatch
	RegisterFuncMap["contains-dossiernr"] = FilterDossiernrContains
	RegisterFuncMap["startswith-dossiernr"] = FilterDossiernrStartsWith
	RegisterGetters["dossiernr"] = GettersDossiernr
	RegisterGroupBy["dossiernr"] = GettersDossiernr

	//register filters for Subdossier
	RegisterFuncMap["match-subdossier"] = FilterSubdossierMatch
	RegisterFuncMap["contains-subdossier"] = FilterSubdossierContains
	RegisterFuncMap["startswith-subdossier"] = FilterSubdossierStartsWith
	RegisterGetters["subdossier"] = GettersSubdossier
	RegisterGroupBy["subdossier"] = GettersSubdossier

	//register filters for Vgnummer
	RegisterFuncMap["match-vgnummer"] = FilterVgnummerMatch
	RegisterFuncMap["contains-vgnummer"] = FilterVgnummerContains
	RegisterFuncMap["startswith-vgnummer"] = FilterVgnummerStartsWith
	RegisterGetters["vgnummer"] = GettersVgnummer
	RegisterGroupBy["vgnummer"] = GettersVgnummer

	//register filters for Hn1X45
	RegisterFuncMap["match-hn_1x45"] = FilterHn1X45Match
	RegisterFuncMap["contains-hn_1x45"] = FilterHn1X45Contains
	RegisterFuncMap["startswith-hn_1x45"] = FilterHn1X45StartsWith
	RegisterGetters["hn_1x45"] = GettersHn1X45
	RegisterGroupBy["hn_1x45"] = GettersHn1X45

	//register filters for Hoofdact
	RegisterFuncMap["match-hoofdact"] = FilterHoofdactMatch
	RegisterFuncMap["contains-hoofdact"] = FilterHoofdactContains
	RegisterFuncMap["startswith-hoofdact"] = FilterHoofdactStartsWith
	RegisterGetters["hoofdact"] = GettersHoofdact
	RegisterGroupBy["hoofdact"] = GettersHoofdact

	//register filters for Nevact1
	RegisterFuncMap["match-nevact1"] = FilterNevact1Match
	RegisterFuncMap["contains-nevact1"] = FilterNevact1Contains
	RegisterFuncMap["startswith-nevact1"] = FilterNevact1StartsWith
	RegisterGetters["nevact1"] = GettersNevact1
	RegisterGroupBy["nevact1"] = GettersNevact1

	//register filters for Nevact2
	RegisterFuncMap["match-nevact2"] = FilterNevact2Match
	RegisterFuncMap["contains-nevact2"] = FilterNevact2Contains
	RegisterFuncMap["startswith-nevact2"] = FilterNevact2StartsWith
	RegisterGetters["nevact2"] = GettersNevact2
	RegisterGroupBy["nevact2"] = GettersNevact2

	//register filters for Huisnr
	RegisterFuncMap["match-huisnr"] = FilterHuisnrMatch
	RegisterFuncMap["contains-huisnr"] = FilterHuisnrContains
	RegisterFuncMap["startswith-huisnr"] = FilterHuisnrStartsWith
	RegisterGetters["huisnr"] = GettersHuisnr
	RegisterGroupBy["huisnr"] = GettersHuisnr

	//register filters for ToevHsnr
	RegisterFuncMap["match-toev_hsnr"] = FilterToevHsnrMatch
	RegisterFuncMap["contains-toev_hsnr"] = FilterToevHsnrContains
	RegisterFuncMap["startswith-toev_hsnr"] = FilterToevHsnrStartsWith
	RegisterGetters["toev_hsnr"] = GettersToevHsnr
	RegisterGroupBy["toev_hsnr"] = GettersToevHsnr

	//register filters for Nm1X45
	RegisterFuncMap["match-nm_1x45"] = FilterNm1X45Match
	RegisterFuncMap["contains-nm_1x45"] = FilterNm1X45Contains
	RegisterFuncMap["startswith-nm_1x45"] = FilterNm1X45StartsWith
	RegisterGetters["nm_1x45"] = GettersNm1X45
	RegisterGroupBy["nm_1x45"] = GettersNm1X45

	//register filters for DatVest
	RegisterFuncMap["match-dat_vest"] = FilterDatVestMatch
	RegisterFuncMap["contains-dat_vest"] = FilterDatVestContains
	RegisterFuncMap["startswith-dat_vest"] = FilterDatVestStartsWith
	RegisterGetters["dat_vest"] = GettersDatVest
	RegisterGroupBy["dat_vest"] = GettersDatVest

	//register filters for DatInschr
	RegisterFuncMap["match-dat_inschr"] = FilterDatInschrMatch
	RegisterFuncMap["contains-dat_inschr"] = FilterDatInschrContains
	RegisterFuncMap["startswith-dat_inschr"] = FilterDatInschrStartsWith
	RegisterGetters["dat_inschr"] = GettersDatInschr
	RegisterGroupBy["dat_inschr"] = GettersDatInschr

	//register filters for DatOprich
	RegisterFuncMap["match-dat_oprich"] = FilterDatOprichMatch
	RegisterFuncMap["contains-dat_oprich"] = FilterDatOprichContains
	RegisterFuncMap["startswith-dat_oprich"] = FilterDatOprichStartsWith
	RegisterGetters["dat_oprich"] = GettersDatOprich
	RegisterGroupBy["dat_oprich"] = GettersDatOprich

	//register filters for RedInschr
	RegisterFuncMap["match-red_inschr"] = FilterRedInschrMatch
	RegisterFuncMap["contains-red_inschr"] = FilterRedInschrContains
	RegisterFuncMap["startswith-red_inschr"] = FilterRedInschrStartsWith
	RegisterGetters["red_inschr"] = GettersRedInschr
	RegisterGroupBy["red_inschr"] = GettersRedInschr

	//register filters for RedUitsch
	RegisterFuncMap["match-red_uitsch"] = FilterRedUitschMatch
	RegisterFuncMap["contains-red_uitsch"] = FilterRedUitschContains
	RegisterFuncMap["startswith-red_uitsch"] = FilterRedUitschStartsWith
	RegisterGetters["red_uitsch"] = GettersRedUitsch
	RegisterGroupBy["red_uitsch"] = GettersRedUitsch

	//register filters for RedOpheff
	RegisterFuncMap["match-red_opheff"] = FilterRedOpheffMatch
	RegisterFuncMap["contains-red_opheff"] = FilterRedOpheffContains
	RegisterFuncMap["startswith-red_opheff"] = FilterRedOpheffStartsWith
	RegisterGetters["red_opheff"] = GettersRedOpheff
	RegisterGroupBy["red_opheff"] = GettersRedOpheff

	//register filters for Vennaam
	RegisterFuncMap["match-vennaam"] = FilterVennaamMatch
	RegisterFuncMap["contains-vennaam"] = FilterVennaamContains
	RegisterFuncMap["startswith-vennaam"] = FilterVennaamStartsWith
	RegisterGetters["vennaam"] = GettersVennaam
	RegisterGroupBy["vennaam"] = GettersVennaam

	//register filters for Gestortkap
	RegisterFuncMap["match-gestortkap"] = FilterGestortkapMatch
	RegisterFuncMap["contains-gestortkap"] = FilterGestortkapContains
	RegisterFuncMap["startswith-gestortkap"] = FilterGestortkapStartsWith
	RegisterGetters["gestortkap"] = GettersGestortkap
	RegisterGroupBy["gestortkap"] = GettersGestortkap

	//register filters for StatInsch
	RegisterFuncMap["match-stat_insch"] = FilterStatInschMatch
	RegisterFuncMap["contains-stat_insch"] = FilterStatInschContains
	RegisterFuncMap["startswith-stat_insch"] = FilterStatInschStartsWith
	RegisterGetters["stat_insch"] = GettersStatInsch
	RegisterGroupBy["stat_insch"] = GettersStatInsch

	//register filters for RvFijn
	RegisterFuncMap["match-rv_fijn"] = FilterRvFijnMatch
	RegisterFuncMap["contains-rv_fijn"] = FilterRvFijnContains
	RegisterFuncMap["startswith-rv_fijn"] = FilterRvFijnStartsWith
	RegisterGetters["rv_fijn"] = GettersRvFijn
	RegisterGroupBy["rv_fijn"] = GettersRvFijn

	//register filters for Url
	RegisterFuncMap["match-url"] = FilterUrlMatch
	RegisterFuncMap["contains-url"] = FilterUrlContains
	RegisterFuncMap["startswith-url"] = FilterUrlStartsWith
	RegisterGetters["url"] = GettersUrl
	RegisterGroupBy["url"] = GettersUrl

	//register filters for Pid
	RegisterFuncMap["match-pid"] = FilterPidMatch
	RegisterFuncMap["contains-pid"] = FilterPidContains
	RegisterFuncMap["startswith-pid"] = FilterPidStartsWith
	RegisterGetters["pid"] = GettersPid
	RegisterGroupBy["pid"] = GettersPid

	//register filters for Numid
	RegisterFuncMap["match-numid"] = FilterNumidMatch
	RegisterFuncMap["contains-numid"] = FilterNumidContains
	RegisterFuncMap["startswith-numid"] = FilterNumidStartsWith
	RegisterGetters["numid"] = GettersNumid
	RegisterGroupBy["numid"] = GettersNumid

	//register filters for Vid
	RegisterFuncMap["match-vid"] = FilterVidMatch
	RegisterFuncMap["contains-vid"] = FilterVidContains
	RegisterFuncMap["startswith-vid"] = FilterVidStartsWith
	RegisterGetters["vid"] = GettersVid
	RegisterGroupBy["vid"] = GettersVid

	//register filters for Sid
	RegisterFuncMap["match-sid"] = FilterSidMatch
	RegisterFuncMap["contains-sid"] = FilterSidContains
	RegisterFuncMap["startswith-sid"] = FilterSidStartsWith
	RegisterGetters["sid"] = GettersSid
	RegisterGroupBy["sid"] = GettersSid

	//register filters for Lid
	RegisterFuncMap["match-lid"] = FilterLidMatch
	RegisterFuncMap["contains-lid"] = FilterLidContains
	RegisterFuncMap["startswith-lid"] = FilterLidStartsWith
	RegisterGetters["lid"] = GettersLid
	RegisterGroupBy["lid"] = GettersLid

	//register filters for Huisnummer
	RegisterFuncMap["match-huisnummer"] = FilterHuisnummerMatch
	RegisterFuncMap["contains-huisnummer"] = FilterHuisnummerContains
	RegisterFuncMap["startswith-huisnummer"] = FilterHuisnummerStartsWith
	RegisterGetters["huisnummer"] = GettersHuisnummer
	RegisterGroupBy["huisnummer"] = GettersHuisnummer

	//register filters for Huisletter
	RegisterFuncMap["match-huisletter"] = FilterHuisletterMatch
	RegisterFuncMap["contains-huisletter"] = FilterHuisletterContains
	RegisterFuncMap["startswith-huisletter"] = FilterHuisletterStartsWith
	RegisterGetters["huisletter"] = GettersHuisletter
	RegisterGroupBy["huisletter"] = GettersHuisletter

	//register filters for Huisnummertoevoeging
	RegisterFuncMap["match-huisnummertoevoeging"] = FilterHuisnummertoevoegingMatch
	RegisterFuncMap["contains-huisnummertoevoeging"] = FilterHuisnummertoevoegingContains
	RegisterFuncMap["startswith-huisnummertoevoeging"] = FilterHuisnummertoevoegingStartsWith
	RegisterGetters["huisnummertoevoeging"] = GettersHuisnummertoevoeging
	RegisterGroupBy["huisnummertoevoeging"] = GettersHuisnummertoevoeging

	//register filters for Postcode
	RegisterFuncMap["match-postcode"] = FilterPostcodeMatch
	RegisterFuncMap["contains-postcode"] = FilterPostcodeContains
	RegisterFuncMap["startswith-postcode"] = FilterPostcodeStartsWith
	RegisterGetters["postcode"] = GettersPostcode
	RegisterGroupBy["postcode"] = GettersPostcode

	//register filters for Oppervlakte
	RegisterFuncMap["match-oppervlakte"] = FilterOppervlakteMatch
	RegisterFuncMap["contains-oppervlakte"] = FilterOppervlakteContains
	RegisterFuncMap["startswith-oppervlakte"] = FilterOppervlakteStartsWith
	RegisterGetters["oppervlakte"] = GettersOppervlakte
	RegisterGroupBy["oppervlakte"] = GettersOppervlakte

	//register filters for MatchScore
	RegisterFuncMap["match-match_score"] = FilterMatchScoreMatch
	RegisterFuncMap["contains-match_score"] = FilterMatchScoreContains
	RegisterFuncMap["startswith-match_score"] = FilterMatchScoreStartsWith
	RegisterGetters["match_score"] = GettersMatchScore
	RegisterGroupBy["match_score"] = GettersMatchScore

	//register filters for Geopunt
	RegisterFuncMap["match-geopunt"] = FilterGeopuntMatch
	RegisterFuncMap["contains-geopunt"] = FilterGeopuntContains
	RegisterFuncMap["startswith-geopunt"] = FilterGeopuntStartsWith
	RegisterGetters["geopunt"] = GettersGeopunt
	RegisterGroupBy["geopunt"] = GettersGeopunt

	//register filters for Buurtnaam
	RegisterFuncMap["match-buurtnaam"] = FilterBuurtnaamMatch
	RegisterFuncMap["contains-buurtnaam"] = FilterBuurtnaamContains
	RegisterFuncMap["startswith-buurtnaam"] = FilterBuurtnaamStartsWith
	RegisterGetters["buurtnaam"] = GettersBuurtnaam
	RegisterGroupBy["buurtnaam"] = GettersBuurtnaam

	//register filters for Buurtcode
	RegisterFuncMap["match-buurtcode"] = FilterBuurtcodeMatch
	RegisterFuncMap["contains-buurtcode"] = FilterBuurtcodeContains
	RegisterFuncMap["startswith-buurtcode"] = FilterBuurtcodeStartsWith
	RegisterGetters["buurtcode"] = GettersBuurtcode
	RegisterGroupBy["buurtcode"] = GettersBuurtcode

	//register filters for Gemeentenaam
	RegisterFuncMap["match-gemeentenaam"] = FilterGemeentenaamMatch
	RegisterFuncMap["contains-gemeentenaam"] = FilterGemeentenaamContains
	RegisterFuncMap["startswith-gemeentenaam"] = FilterGemeentenaamStartsWith
	RegisterGetters["gemeentenaam"] = GettersGemeentenaam
	RegisterGroupBy["gemeentenaam"] = GettersGemeentenaam

	//register filters for Gemeentecode
	RegisterFuncMap["match-gemeentecode"] = FilterGemeentecodeMatch
	RegisterFuncMap["contains-gemeentecode"] = FilterGemeentecodeContains
	RegisterFuncMap["startswith-gemeentecode"] = FilterGemeentecodeStartsWith
	RegisterGetters["gemeentecode"] = GettersGemeentecode
	RegisterGroupBy["gemeentecode"] = GettersGemeentecode

	//register filters for Provincienaam
	RegisterFuncMap["match-provincienaam"] = FilterProvincienaamMatch
	RegisterFuncMap["contains-provincienaam"] = FilterProvincienaamContains
	RegisterFuncMap["startswith-provincienaam"] = FilterProvincienaamStartsWith
	RegisterGetters["provincienaam"] = GettersProvincienaam
	RegisterGroupBy["provincienaam"] = GettersProvincienaam

	//register filters for Provinciecode
	RegisterFuncMap["match-provinciecode"] = FilterProvinciecodeMatch
	RegisterFuncMap["contains-provinciecode"] = FilterProvinciecodeContains
	RegisterFuncMap["startswith-provinciecode"] = FilterProvinciecodeStartsWith
	RegisterGetters["provinciecode"] = GettersProvinciecode
	RegisterGroupBy["provinciecode"] = GettersProvinciecode

	//register filters for OmschrijvingRechtsvormcode
	RegisterFuncMap["match-omschrijving_rechtsvormcode"] = FilterOmschrijvingRechtsvormcodeMatch
	RegisterFuncMap["contains-omschrijving_rechtsvormcode"] = FilterOmschrijvingRechtsvormcodeContains
	RegisterFuncMap["startswith-omschrijving_rechtsvormcode"] = FilterOmschrijvingRechtsvormcodeStartsWith
	RegisterGetters["omschrijving_rechtsvormcode"] = GettersOmschrijvingRechtsvormcode
	RegisterGroupBy["omschrijving_rechtsvormcode"] = GettersOmschrijvingRechtsvormcode

	//register filters for Geometry
	// RegisterFuncMap["match-geometry"] = FilterGeometryMatch
	// RegisterFuncMap["contains-geometry"] = FilterGeometryContains
	// RegisterFuncMap["startswith-geometry"] = FilterGeometryStartsWith
	// RegisterGetters["geometry"] = GettersGeometry
	// RegisterGroupBy["geometry"] = GettersGeometry

	//register filters for Sbicode
	RegisterFuncMap["match-sbicode"] = FilterSbicodeMatch
	RegisterFuncMap["contains-sbicode"] = FilterSbicodeContains
	RegisterFuncMap["startswith-sbicode"] = FilterSbicodeStartsWith
	RegisterGetters["sbicode"] = GettersSbicode
	RegisterGroupBy["sbicode"] = GettersSbicode

	//register filters for Activiteit
	RegisterFuncMap["match-activiteit"] = FilterActiviteitMatch
	RegisterFuncMap["contains-activiteit"] = FilterActiviteitContains
	RegisterFuncMap["startswith-activiteit"] = FilterActiviteitStartsWith
	RegisterGetters["activiteit"] = GettersActiviteit
	RegisterGroupBy["activiteit"] = GettersActiviteit

	//register filters for L1Code
	RegisterFuncMap["match-l1_code"] = FilterL1CodeMatch
	RegisterFuncMap["contains-l1_code"] = FilterL1CodeContains
	RegisterFuncMap["startswith-l1_code"] = FilterL1CodeStartsWith
	RegisterGetters["l1_code"] = GettersL1Code
	RegisterGroupBy["l1_code"] = GettersL1Code

	//register filters for L1Title
	RegisterFuncMap["match-l1_title"] = FilterL1TitleMatch
	RegisterFuncMap["contains-l1_title"] = FilterL1TitleContains
	RegisterFuncMap["startswith-l1_title"] = FilterL1TitleStartsWith
	RegisterGetters["l1_title"] = GettersL1Title
	RegisterGroupBy["l1_title"] = GettersL1Title

	//register filters for L2Code
	RegisterFuncMap["match-l2_code"] = FilterL2CodeMatch
	RegisterFuncMap["contains-l2_code"] = FilterL2CodeContains
	RegisterFuncMap["startswith-l2_code"] = FilterL2CodeStartsWith
	RegisterGetters["l2_code"] = GettersL2Code
	RegisterGroupBy["l2_code"] = GettersL2Code

	//register filters for L2Title
	RegisterFuncMap["match-l2_title"] = FilterL2TitleMatch
	RegisterFuncMap["contains-l2_title"] = FilterL2TitleContains
	RegisterFuncMap["startswith-l2_title"] = FilterL2TitleStartsWith
	RegisterGetters["l2_title"] = GettersL2Title
	RegisterGroupBy["l2_title"] = GettersL2Title

	//register filters for L3Code
	RegisterFuncMap["match-l3_code"] = FilterL3CodeMatch
	RegisterFuncMap["contains-l3_code"] = FilterL3CodeContains
	RegisterFuncMap["startswith-l3_code"] = FilterL3CodeStartsWith
	RegisterGetters["l3_code"] = GettersL3Code
	RegisterGroupBy["l3_code"] = GettersL3Code

	//register filters for L3Title
	RegisterFuncMap["match-l3_title"] = FilterL3TitleMatch
	RegisterFuncMap["contains-l3_title"] = FilterL3TitleContains
	RegisterFuncMap["startswith-l3_title"] = FilterL3TitleStartsWith
	RegisterGetters["l3_title"] = GettersL3Title
	RegisterGroupBy["l3_title"] = GettersL3Title

	//register filters for L4Code
	RegisterFuncMap["match-l4_code"] = FilterL4CodeMatch
	RegisterFuncMap["contains-l4_code"] = FilterL4CodeContains
	RegisterFuncMap["startswith-l4_code"] = FilterL4CodeStartsWith
	RegisterGetters["l4_code"] = GettersL4Code
	RegisterGroupBy["l4_code"] = GettersL4Code

	//register filters for L4Title
	RegisterFuncMap["match-l4_title"] = FilterL4TitleMatch
	RegisterFuncMap["contains-l4_title"] = FilterL4TitleContains
	RegisterFuncMap["startswith-l4_title"] = FilterL4TitleStartsWith
	RegisterGetters["l4_title"] = GettersL4Title
	RegisterGroupBy["l4_title"] = GettersL4Title

	//register filters for L5Code
	RegisterFuncMap["match-l5_code"] = FilterL5CodeMatch
	RegisterFuncMap["contains-l5_code"] = FilterL5CodeContains
	RegisterFuncMap["startswith-l5_code"] = FilterL5CodeStartsWith
	RegisterGetters["l5_code"] = GettersL5Code
	RegisterGroupBy["l5_code"] = GettersL5Code

	//register filters for L5Title
	RegisterFuncMap["match-l5_title"] = FilterL5TitleMatch
	RegisterFuncMap["contains-l5_title"] = FilterL5TitleContains
	RegisterFuncMap["startswith-l5_title"] = FilterL5TitleStartsWith
	RegisterGetters["l5_title"] = GettersL5Title
	RegisterGroupBy["l5_title"] = GettersL5Title

	//register filters for Id
	RegisterFuncMap["match-id"] = FilterIdMatch
	RegisterFuncMap["contains-id"] = FilterIdContains
	RegisterFuncMap["startswith-id"] = FilterIdStartsWith
	RegisterGetters["id"] = GettersId
	RegisterGroupBy["id"] = GettersId

	validateRegisters()

	/*
		RegisterFuncMap["match-ekey"] = FilterEkeyMatch
		RegisterFuncMap["contains-ekey"] = FilterEkeyContains
		// register startswith filters
		RegisterFuncMap["startswith-ekey"] = FilterEkeyStartsWith
		// register getters
		RegisterGetters["ekey"] = GettersEkey
		// register groupby
		RegisterGroupBy["ekey"] = GettersEkey

	*/

	// register reduce functions
	RegisterReduce["count"] = reduceCount
	RegisterReduce["vestigingen"] = reduceVestigingen
}

type sortLookup map[string]func(int, int) bool

func createSort(items Items) sortLookup {

	sortFuncs := sortLookup{

		"dossiernr":  func(i, j int) bool { return items[i].Dossiernr < items[j].Dossiernr },
		"-dossiernr": func(i, j int) bool { return items[i].Dossiernr > items[j].Dossiernr },

		"subdossier":  func(i, j int) bool { return items[i].Subdossier < items[j].Subdossier },
		"-subdossier": func(i, j int) bool { return items[i].Subdossier > items[j].Subdossier },

		"vgnummer":  func(i, j int) bool { return items[i].Vgnummer < items[j].Vgnummer },
		"-vgnummer": func(i, j int) bool { return items[i].Vgnummer > items[j].Vgnummer },

		"hn_1x45":  func(i, j int) bool { return items[i].Hn1X45 < items[j].Hn1X45 },
		"-hn_1x45": func(i, j int) bool { return items[i].Hn1X45 > items[j].Hn1X45 },

		"hoofdact": func(i, j int) bool {
			return Hoofdact.GetValue(items[i].Hoofdact) < Hoofdact.GetValue(items[j].Hoofdact)
		},
		"-hoofdact": func(i, j int) bool {
			return Hoofdact.GetValue(items[i].Hoofdact) > Hoofdact.GetValue(items[j].Hoofdact)
		},

		"nevact1":  func(i, j int) bool { return Nevact1.GetValue(items[i].Nevact1) < Nevact1.GetValue(items[j].Nevact1) },
		"-nevact1": func(i, j int) bool { return Nevact1.GetValue(items[i].Nevact1) > Nevact1.GetValue(items[j].Nevact1) },

		"nevact2":  func(i, j int) bool { return Nevact2.GetValue(items[i].Nevact2) < Nevact2.GetValue(items[j].Nevact2) },
		"-nevact2": func(i, j int) bool { return Nevact2.GetValue(items[i].Nevact2) > Nevact2.GetValue(items[j].Nevact2) },

		"huisnr":  func(i, j int) bool { return items[i].Huisnr < items[j].Huisnr },
		"-huisnr": func(i, j int) bool { return items[i].Huisnr > items[j].Huisnr },

		"toev_hsnr":  func(i, j int) bool { return items[i].ToevHsnr < items[j].ToevHsnr },
		"-toev_hsnr": func(i, j int) bool { return items[i].ToevHsnr > items[j].ToevHsnr },

		"nm_1x45":  func(i, j int) bool { return items[i].Nm1X45 < items[j].Nm1X45 },
		"-nm_1x45": func(i, j int) bool { return items[i].Nm1X45 > items[j].Nm1X45 },

		"dat_vest":  func(i, j int) bool { return items[i].DatVest < items[j].DatVest },
		"-dat_vest": func(i, j int) bool { return items[i].DatVest > items[j].DatVest },

		"dat_inschr":  func(i, j int) bool { return items[i].DatInschr < items[j].DatInschr },
		"-dat_inschr": func(i, j int) bool { return items[i].DatInschr > items[j].DatInschr },

		"dat_oprich":  func(i, j int) bool { return items[i].DatOprich < items[j].DatOprich },
		"-dat_oprich": func(i, j int) bool { return items[i].DatOprich > items[j].DatOprich },

		"red_inschr":  func(i, j int) bool { return items[i].RedInschr < items[j].RedInschr },
		"-red_inschr": func(i, j int) bool { return items[i].RedInschr > items[j].RedInschr },

		"red_uitsch":  func(i, j int) bool { return items[i].RedUitsch < items[j].RedUitsch },
		"-red_uitsch": func(i, j int) bool { return items[i].RedUitsch > items[j].RedUitsch },

		"red_opheff":  func(i, j int) bool { return items[i].RedOpheff < items[j].RedOpheff },
		"-red_opheff": func(i, j int) bool { return items[i].RedOpheff > items[j].RedOpheff },

		"vennaam":  func(i, j int) bool { return items[i].Vennaam < items[j].Vennaam },
		"-vennaam": func(i, j int) bool { return items[i].Vennaam > items[j].Vennaam },

		"gestortkap":  func(i, j int) bool { return items[i].Gestortkap < items[j].Gestortkap },
		"-gestortkap": func(i, j int) bool { return items[i].Gestortkap > items[j].Gestortkap },

		"stat_insch":  func(i, j int) bool { return items[i].StatInsch < items[j].StatInsch },
		"-stat_insch": func(i, j int) bool { return items[i].StatInsch > items[j].StatInsch },

		"rv_fijn":  func(i, j int) bool { return RvFijn.GetValue(items[i].RvFijn) < RvFijn.GetValue(items[j].RvFijn) },
		"-rv_fijn": func(i, j int) bool { return RvFijn.GetValue(items[i].RvFijn) > RvFijn.GetValue(items[j].RvFijn) },

		"url":  func(i, j int) bool { return items[i].Url < items[j].Url },
		"-url": func(i, j int) bool { return items[i].Url > items[j].Url },

		"pid":  func(i, j int) bool { return items[i].Pid < items[j].Pid },
		"-pid": func(i, j int) bool { return items[i].Pid > items[j].Pid },

		"numid":  func(i, j int) bool { return items[i].Numid < items[j].Numid },
		"-numid": func(i, j int) bool { return items[i].Numid > items[j].Numid },

		"vid":  func(i, j int) bool { return items[i].Vid < items[j].Vid },
		"-vid": func(i, j int) bool { return items[i].Vid > items[j].Vid },

		"sid":  func(i, j int) bool { return items[i].Sid < items[j].Sid },
		"-sid": func(i, j int) bool { return items[i].Sid > items[j].Sid },

		"lid":  func(i, j int) bool { return items[i].Lid < items[j].Lid },
		"-lid": func(i, j int) bool { return items[i].Lid > items[j].Lid },

		"huisnummer":  func(i, j int) bool { return items[i].Huisnummer < items[j].Huisnummer },
		"-huisnummer": func(i, j int) bool { return items[i].Huisnummer > items[j].Huisnummer },

		"huisletter":  func(i, j int) bool { return items[i].Huisletter < items[j].Huisletter },
		"-huisletter": func(i, j int) bool { return items[i].Huisletter > items[j].Huisletter },

		"huisnummertoevoeging":  func(i, j int) bool { return items[i].Huisnummertoevoeging < items[j].Huisnummertoevoeging },
		"-huisnummertoevoeging": func(i, j int) bool { return items[i].Huisnummertoevoeging > items[j].Huisnummertoevoeging },

		"postcode":  func(i, j int) bool { return items[i].Postcode < items[j].Postcode },
		"-postcode": func(i, j int) bool { return items[i].Postcode > items[j].Postcode },

		"oppervlakte":  func(i, j int) bool { return items[i].Oppervlakte < items[j].Oppervlakte },
		"-oppervlakte": func(i, j int) bool { return items[i].Oppervlakte > items[j].Oppervlakte },

		"match_score":  func(i, j int) bool { return items[i].MatchScore < items[j].MatchScore },
		"-match_score": func(i, j int) bool { return items[i].MatchScore > items[j].MatchScore },

		"geopunt":  func(i, j int) bool { return items[i].Geopunt < items[j].Geopunt },
		"-geopunt": func(i, j int) bool { return items[i].Geopunt > items[j].Geopunt },

		"buurtnaam": func(i, j int) bool {
			return Buurtnaam.GetValue(items[i].Buurtnaam) < Buurtnaam.GetValue(items[j].Buurtnaam)
		},
		"-buurtnaam": func(i, j int) bool {
			return Buurtnaam.GetValue(items[i].Buurtnaam) > Buurtnaam.GetValue(items[j].Buurtnaam)
		},

		"buurtcode": func(i, j int) bool {
			return Buurtcode.GetValue(items[i].Buurtcode) < Buurtcode.GetValue(items[j].Buurtcode)
		},
		"-buurtcode": func(i, j int) bool {
			return Buurtcode.GetValue(items[i].Buurtcode) > Buurtcode.GetValue(items[j].Buurtcode)
		},

		"gemeentenaam": func(i, j int) bool {
			return Gemeentenaam.GetValue(items[i].Gemeentenaam) < Gemeentenaam.GetValue(items[j].Gemeentenaam)
		},
		"-gemeentenaam": func(i, j int) bool {
			return Gemeentenaam.GetValue(items[i].Gemeentenaam) > Gemeentenaam.GetValue(items[j].Gemeentenaam)
		},

		"gemeentecode": func(i, j int) bool {
			return Gemeentecode.GetValue(items[i].Gemeentecode) < Gemeentecode.GetValue(items[j].Gemeentecode)
		},
		"-gemeentecode": func(i, j int) bool {
			return Gemeentecode.GetValue(items[i].Gemeentecode) > Gemeentecode.GetValue(items[j].Gemeentecode)
		},

		"provincienaam": func(i, j int) bool {
			return Provincienaam.GetValue(items[i].Provincienaam) < Provincienaam.GetValue(items[j].Provincienaam)
		},
		"-provincienaam": func(i, j int) bool {
			return Provincienaam.GetValue(items[i].Provincienaam) > Provincienaam.GetValue(items[j].Provincienaam)
		},

		"provinciecode": func(i, j int) bool {
			return Provinciecode.GetValue(items[i].Provinciecode) < Provinciecode.GetValue(items[j].Provinciecode)
		},
		"-provinciecode": func(i, j int) bool {
			return Provinciecode.GetValue(items[i].Provinciecode) > Provinciecode.GetValue(items[j].Provinciecode)
		},

		"omschrijving_rechtsvormcode": func(i, j int) bool {
			return OmschrijvingRechtsvormcode.GetValue(items[i].OmschrijvingRechtsvormcode) < OmschrijvingRechtsvormcode.GetValue(items[j].OmschrijvingRechtsvormcode)
		},
		"-omschrijving_rechtsvormcode": func(i, j int) bool {
			return OmschrijvingRechtsvormcode.GetValue(items[i].OmschrijvingRechtsvormcode) > OmschrijvingRechtsvormcode.GetValue(items[j].OmschrijvingRechtsvormcode)
		},

		"sbicode":  func(i, j int) bool { return Sbicode.GetValue(items[i].Sbicode) < Sbicode.GetValue(items[j].Sbicode) },
		"-sbicode": func(i, j int) bool { return Sbicode.GetValue(items[i].Sbicode) > Sbicode.GetValue(items[j].Sbicode) },

		"activiteit": func(i, j int) bool {
			return Activiteit.GetValue(items[i].Activiteit) < Activiteit.GetValue(items[j].Activiteit)
		},
		"-activiteit": func(i, j int) bool {
			return Activiteit.GetValue(items[i].Activiteit) > Activiteit.GetValue(items[j].Activiteit)
		},

		"l1_code":  func(i, j int) bool { return L1Code.GetValue(items[i].L1Code) < L1Code.GetValue(items[j].L1Code) },
		"-l1_code": func(i, j int) bool { return L1Code.GetValue(items[i].L1Code) > L1Code.GetValue(items[j].L1Code) },

		"l1_title":  func(i, j int) bool { return L1Title.GetValue(items[i].L1Title) < L1Title.GetValue(items[j].L1Title) },
		"-l1_title": func(i, j int) bool { return L1Title.GetValue(items[i].L1Title) > L1Title.GetValue(items[j].L1Title) },

		"l2_code":  func(i, j int) bool { return L2Code.GetValue(items[i].L2Code) < L2Code.GetValue(items[j].L2Code) },
		"-l2_code": func(i, j int) bool { return L2Code.GetValue(items[i].L2Code) > L2Code.GetValue(items[j].L2Code) },

		"l2_title":  func(i, j int) bool { return L2Title.GetValue(items[i].L2Title) < L2Title.GetValue(items[j].L2Title) },
		"-l2_title": func(i, j int) bool { return L2Title.GetValue(items[i].L2Title) > L2Title.GetValue(items[j].L2Title) },

		"l3_code":  func(i, j int) bool { return L3Code.GetValue(items[i].L3Code) < L3Code.GetValue(items[j].L3Code) },
		"-l3_code": func(i, j int) bool { return L3Code.GetValue(items[i].L3Code) > L3Code.GetValue(items[j].L3Code) },

		"l3_title":  func(i, j int) bool { return L3Title.GetValue(items[i].L3Title) < L3Title.GetValue(items[j].L3Title) },
		"-l3_title": func(i, j int) bool { return L3Title.GetValue(items[i].L3Title) > L3Title.GetValue(items[j].L3Title) },

		"l4_code":  func(i, j int) bool { return L4Code.GetValue(items[i].L4Code) < L4Code.GetValue(items[j].L4Code) },
		"-l4_code": func(i, j int) bool { return L4Code.GetValue(items[i].L4Code) > L4Code.GetValue(items[j].L4Code) },

		"l4_title":  func(i, j int) bool { return L4Title.GetValue(items[i].L4Title) < L4Title.GetValue(items[j].L4Title) },
		"-l4_title": func(i, j int) bool { return L4Title.GetValue(items[i].L4Title) > L4Title.GetValue(items[j].L4Title) },

		"l5_code":  func(i, j int) bool { return L5Code.GetValue(items[i].L5Code) < L5Code.GetValue(items[j].L5Code) },
		"-l5_code": func(i, j int) bool { return L5Code.GetValue(items[i].L5Code) > L5Code.GetValue(items[j].L5Code) },

		"l5_title":  func(i, j int) bool { return L5Title.GetValue(items[i].L5Title) < L5Title.GetValue(items[j].L5Title) },
		"-l5_title": func(i, j int) bool { return L5Title.GetValue(items[i].L5Title) > L5Title.GetValue(items[j].L5Title) },

		"id":  func(i, j int) bool { return items[i].Id < items[j].Id },
		"-id": func(i, j int) bool { return items[i].Id > items[j].Id },
	}
	return sortFuncs
}

func sortBy(items Items, sortingL []string) (Items, []string) {
	sortFuncs := createSort(items)

	for _, sortFuncName := range sortingL {
		sortFunc, ok := sortFuncs[sortFuncName]
		if ok {
			sort.Slice(items, sortFunc)
		}
	}

	// TODO must be nicer way
	keys := []string{}
	for key := range sortFuncs {
		keys = append(keys, key)
	}

	return items, keys
}
