package main

import (
	"database/sql"
	"fmt"
	"log"
	"time"

	_ "github.com/lib/pq"
)

// lastTableComment is used to keep track
// of update on database source table.
var lastTableComment string
var sourceTable = "kvk.kvk_expand_sbi"

// ConnectStr create string to connect to database
func ConnectStr() string {
	otherParams := "connect_timeout=5"
	return fmt.Sprintf(
		"user=%s dbname=%s password='%s' host=%s port=%s sslmode=%s  %s",
		SETTINGS.Get("PGUSER"),
		SETTINGS.Get("PGDATABASE"),
		SETTINGS.Get("PGPASSWORD"),
		SETTINGS.Get("PGHOST"),
		SETTINGS.Get("PGPORT"),
		SETTINGS.Get("PGSSLMODE"),
		otherParams,
	)
}

func dbConnect(connStr string) (*sql.DB, error) {
	//connStr := connectStr()
	db, err := sql.Open("postgres", connStr)
	if err != nil {
		return db, err
	}

	err = db.Ping()
	return db, err
}

type Comment struct {
	Comment sql.NullString
}

/*
 * source database table should
 * have comment if comment is
 * changed we reload data.
 *
 */
func tableComment() (string, error) {
	db, err := dbConnect(ConnectStr())

	if err != nil {
		log.Println(err)
		return "", err
	}

	defer db.Close()

	query := fmt.Sprintf(`
	select obj_description('%s'::regclass);
	`, sourceTable)

	rows, err := db.Query(query)

	if err != nil {
		log.Println(err)
		return "", err
	}

	c := Comment{}

	for rows.Next() {
		if err := rows.Scan(&(c).Comment); err != nil {
			// Check for a scan error.
			// Query rows will be closed with defer.
			log.Println(err)
			return "", err
		}
	}
	comment := convertSqlNullString(c.Comment)
	// fmt.Printf("%s %s \n", sourceTable, comment)
	return comment, nil
}

// Every x seconds check database for changed comment.
// if changed replace data in ITEMS
//
// attempt reload if data is missing
// crash if S2CELSS are empty.
func CheckForChange() {

	uptimeTicker := time.NewTicker(35 * time.Second)
	validateTicker := time.NewTicker(3600 * time.Second)
	failTicker := time.NewTicker(3800 * time.Second)

	for {
		select {
		case <-uptimeTicker.C:
			newComment, err := tableComment()
			if err == nil && newComment != lastTableComment {
				if newComment != "" {
					reset()
					fillFromDB(itemChan)
				}
			}
		case <-validateTicker.C:
			if len(ITEMS) < 100 {
				reset()
				fillFromDB(itemChan)
			}
		case <-failTicker.C:
			if len(S2CELLS) < 100 {
				log.Fatal("missing geo data, force crash")
			}
		}
	}
}

type ItemInSQL struct {
	Dossiernr                  sql.NullString
	Subdossier                 sql.NullString
	Vgnummer                   sql.NullString
	Hn1X45                     sql.NullString
	Hoofdact                   sql.NullString
	Nevact1                    sql.NullString
	Nevact2                    sql.NullString
	Huisnr                     sql.NullString
	ToevHsnr                   sql.NullString
	Nm1X45                     sql.NullString
	DatVest                    sql.NullString
	DatInschr                  sql.NullString
	DatOprich                  sql.NullString
	RedInschr                  sql.NullString
	RedUitsch                  sql.NullString
	RedOpheff                  sql.NullString
	Vennaam                    sql.NullString
	Gestortkap                 sql.NullString
	StatInsch                  sql.NullString
	MobielNr                   sql.NullString
	RvFijn                     sql.NullString
	Url                        sql.NullString
	Pid                        sql.NullString
	Numid                      sql.NullString
	Vid                        sql.NullString
	Sid                        sql.NullString
	Lid                        sql.NullString
	Huisnummer                 sql.NullString
	Huisletter                 sql.NullString
	Huisnummertoevoeging       sql.NullString
	Postcode                   sql.NullString
	Oppervlakte                sql.NullString
	MatchScore                 sql.NullString
	Geopunt                    sql.NullString
	Buurtnaam                  sql.NullString
	Buurtcode                  sql.NullString
	Gemeentenaam               sql.NullString
	Gemeentecode               sql.NullString
	Provincienaam              sql.NullString
	Provinciecode              sql.NullString
	OmschrijvingRechtsvormcode sql.NullString
	Sbicode                    sql.NullString
	Activiteit                 sql.NullString
	L1Code                     sql.NullString
	L1Title                    sql.NullString
	L2Code                     sql.NullString
	L2Title                    sql.NullString
	L3Code                     sql.NullString
	L3Title                    sql.NullString
	L4Code                     sql.NullString
	L4Title                    sql.NullString
	L5Code                     sql.NullString
	L5Title                    sql.NullString
	Id                         sql.NullString
}

func fillFromDB(items ItemsChannel) {
	// update last know comment from source table
	// so we can detect changes.
	comment, err := tableComment()

	if err != nil {
		log.Fatal(err)
	}

	lastTableComment = comment

	db, err := dbConnect(ConnectStr())
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	query := fmt.Sprintf(`
	SELECT
		dossiernr,
		subdossier,
		vgnummer,
		hn_1x45,
		hoofdact,
		nevact1,
		nevact2,
		huisnr,
		toev_hsnr,
		nm_1x45,
		dat_vest,
		dat_inschr,
		dat_oprich,
		red_inschr,
		red_uitsch,
		red_opheff,
		vennaam,
		gestortkap,
		stat_insch,
		mobiel_nr,
		rv_fijn,
		url,
		pid,
		numid,
		vid,
		sid,
		lid,
		huisnummer,
		huisletter,
		huisnummertoevoeging,
		postcode,
		oppervlakte,
		match_score,
		st_astext(st_transform(geometry, 4326)) as geopunt,
		buurtnaam, buurtcode, gemeentenaam, gemeentecode,
		provincienaam, provinciecode,
		omschrijving_rechtsvormcode,
		sbicode, activiteit,
		l1_code, l1_title, l2_code, l2_title, l3_code, l3_title,
		l4_code, l4_title, l5_code, l5_title,
		id
		FROM %s;
 	`, sourceTable)

	rows, err := db.Query(query)

	if err != nil {
		log.Fatal(err)
	}

	itemsArray := ItemsIn{}
	in := ItemInSQL{}

	rowCounter := 0

	for rows.Next() {
		if err := rows.Scan(
			&(in).Dossiernr,
			&(in).Subdossier,
			&(in).Vgnummer,
			&(in).Hn1X45,
			&(in).Hoofdact,
			&(in).Nevact1,
			&(in).Nevact2,
			&(in).Huisnr,
			&(in).ToevHsnr,
			&(in).Nm1X45,
			&(in).DatVest,
			&(in).DatInschr,
			&(in).DatOprich,
			&(in).RedInschr,
			&(in).RedUitsch,
			&(in).RedOpheff,
			&(in).Vennaam,
			&(in).Gestortkap,
			&(in).StatInsch,
			&(in).MobielNr,
			&(in).RvFijn,
			&(in).Url,
			&(in).Pid,
			&(in).Numid,
			&(in).Vid,
			&(in).Sid,
			&(in).Lid,
			&(in).Huisnummer,
			&(in).Huisletter,
			&(in).Huisnummertoevoeging,
			&(in).Postcode,
			&(in).Oppervlakte,
			&(in).MatchScore,
			&(in).Geopunt,
			&(in).Buurtnaam,
			&(in).Buurtcode,
			&(in).Gemeentenaam,
			&(in).Gemeentecode,
			&(in).Provincienaam,
			&(in).Provinciecode,
			&(in).OmschrijvingRechtsvormcode,
			&(in).Sbicode,
			&(in).Activiteit,
			&(in).L1Code,
			&(in).L1Title,
			&(in).L2Code,
			&(in).L2Title,
			&(in).L3Code,
			&(in).L3Title,
			&(in).L4Code,
			&(in).L4Title,
			&(in).L5Code,
			&(in).L5Title,
			&(in).Id,
		); err != nil {
			// Check for a scan error.
			// Query rows will be closed with defer.
			log.Fatal(err)
		}

		itemIn := &ItemIn{
			convertSqlNullString(in.Dossiernr),
			convertSqlNullString(in.Subdossier),
			convertSqlNullString(in.Vgnummer),
			convertSqlNullString(in.Hn1X45),
			convertSqlNullString(in.Hoofdact),
			convertSqlNullString(in.Nevact1),
			convertSqlNullString(in.Nevact2),
			convertSqlNullString(in.Huisnr),
			convertSqlNullString(in.ToevHsnr),
			convertSqlNullString(in.Nm1X45),
			convertSqlNullString(in.DatVest),
			convertSqlNullString(in.DatInschr),
			convertSqlNullString(in.DatOprich),
			convertSqlNullString(in.RedInschr),
			convertSqlNullString(in.RedUitsch),
			convertSqlNullString(in.RedOpheff),
			convertSqlNullString(in.Vennaam),
			convertSqlNullString(in.Gestortkap),
			convertSqlNullString(in.StatInsch),
			convertSqlNullString(in.MobielNr),
			convertSqlNullString(in.RvFijn),
			convertSqlNullString(in.Url),
			convertSqlNullString(in.Pid),
			convertSqlNullString(in.Numid),
			convertSqlNullString(in.Vid),
			convertSqlNullString(in.Sid),
			convertSqlNullString(in.Lid),
			convertSqlNullString(in.Huisnummer),
			convertSqlNullString(in.Huisletter),
			convertSqlNullString(in.Huisnummertoevoeging),
			convertSqlNullString(in.Postcode),
			convertSqlNullString(in.Oppervlakte),
			convertSqlNullString(in.MatchScore),
			convertSqlNullString(in.Geopunt),
			convertSqlNullString(in.Buurtnaam),
			convertSqlNullString(in.Buurtcode),
			convertSqlNullString(in.Gemeentenaam),
			convertSqlNullString(in.Gemeentecode),
			convertSqlNullString(in.Provincienaam),
			convertSqlNullString(in.Provinciecode),
			convertSqlNullString(in.OmschrijvingRechtsvormcode),
			convertSqlNullString(in.Sbicode),
			convertSqlNullString(in.Activiteit),
			convertSqlNullString(in.L1Code),
			convertSqlNullString(in.L1Title),
			convertSqlNullString(in.L2Code),
			convertSqlNullString(in.L2Title),
			convertSqlNullString(in.L3Code),
			convertSqlNullString(in.L3Title),
			convertSqlNullString(in.L4Code),
			convertSqlNullString(in.L4Title),
			convertSqlNullString(in.L5Code),
			convertSqlNullString(in.L5Title),
			convertSqlNullString(in.Id),
		}

		if len(itemsArray) > 1000 {
			itemChan <- itemsArray
			itemsArray = ItemsIn{}
		}

		itemsArray = append(itemsArray, itemIn)
		rowCounter += 1
	}

	// add left over items
	itemChan <- itemsArray

	// If the database is being written to ensure to check for Close
	// errors that may be returned from the driver. The query may
	// encounter an auto-commit error and be forced to rollback changes.
	rerr := rows.Close()

	if rerr != nil {
		log.Fatal(err)
	}
	// Rows.Err will report the last error encountered by Rows.Scan.
	if err := rows.Err(); err != nil {
		log.Fatal(err)
	}

	lock.Lock()
	defer lock.Unlock()

	S2CELLS.Sort()

	log.Printf("added %d rows from database", rowCounter)
}

func convertSqlNullString(v sql.NullString) string {
	if v.Valid {
		return v.String
	} else {
		return ""
	}
}

/*
func convertSqlNullInt(v sql.NullInt64) int64 {
	var err error
	var output []byte

	if v.Valid {
		output, err = json.Marshal(v.Int64)
	} else {
		output, err = json.Marshal(nil)
		return int64(0)
	}

	if err != nil {
		panic(err)
	}

	bla, err := strconv.ParseInt(string(output), 10, 64)

	if err != nil {
		panic(err)
	}

	return bla

}

func convertSqlNullFloat(v sql.NullFloat64) float64 {
	var err error
	var output []byte

	if v.Valid {
		output, err = json.Marshal(v.Float64)
	} else {
		output, err = json.Marshal(nil)
		return float64(0)
	}

	if err != nil {
		panic(err)
	}

	bla, err := strconv.ParseFloat(string(output), 64)

	if err != nil {
		panic(err)
	}

	return bla

}
*/
